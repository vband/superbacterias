﻿using System.Threading.Tasks;
using SuperbacteriasCode.Models;
using UnityEngine;

namespace SuperbacteriasCode.Controllers
{
    public class VictoryController : EntryAnimationBehaviour
    {
        [SerializeField] private GameObject _victoryScreen;
        
        public bool AreAllBacteriasDead(CardRepository bacteriaRepository)
        {
            return bacteriaRepository.Count() == 0;
        }

        public async Task ShowVictoryScreenAsync()
        {
            _victoryScreen.SetActive(true);
            
            await PlayEntryAnimationAsync();
        }
    }
}