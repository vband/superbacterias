﻿using System.Threading.Tasks;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.Base
{
    public abstract class GameStep : MonoBehaviour
    {
        public abstract Task<bool> RunStepLogic();
    }
}