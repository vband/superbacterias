﻿using System.Threading.Tasks;
using SuperbacteriasCode.Controllers.GameSteps.Base;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.Implementation
{
    public class InitializeBacteriasStep : GameStep
    {
        [SerializeField] private BacteriaGenerator _bacteriaGenerator;
        
        public override Task<bool> RunStepLogic()
        {
            _bacteriaGenerator.InitializeBacterias();
            
            return Task.Factory.StartNew(() => true);
        }
    }
}