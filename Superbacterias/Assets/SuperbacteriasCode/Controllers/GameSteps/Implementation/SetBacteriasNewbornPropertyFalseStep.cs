﻿using System.Threading.Tasks;
using SuperbacteriasCode.Controllers.GameSteps.Base;
using SuperbacteriasCode.Models;
using SuperbacteriasCode.Models.Cards;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.Implementation
{
    public class SetBacteriasNewbornPropertyFalseStep : GameStep
    {
        [SerializeField] private CardRepository _bacteriaRepository;
        
        public override Task<bool> RunStepLogic()
        {
            for (var i = 0; i < _bacteriaRepository.Count(); i++)
            {
                var bacteria = _bacteriaRepository.Get(i) as BacteriaCard;

                if (bacteria is null) continue;

                bacteria.Newborn = false;
            }

            return Task.Factory.StartNew(() => true);
        }
    }
}