﻿using System.Threading.Tasks;
using SuperbacteriasCode.Controllers.GameSteps.Base;
using SuperbacteriasCode.Models;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.Implementation
{
    public class CheckAllBacteriasDeadStep : GameStep
    {
        [SerializeField] private VictoryController _victoryController;
        [SerializeField] private CardRepository _bacteriaRepository;
        [SerializeField] private BoolReactiveScriptable _isGameActive;
        
        public override async Task<bool> RunStepLogic()
        {
            if (!AreAllBacteriasDead())
                return true;
            
            _isGameActive.SetValue(false);
            await _victoryController.ShowVictoryScreenAsync();
            return false;

        }
        
        private bool AreAllBacteriasDead()
        {
            return _victoryController.AreAllBacteriasDead(_bacteriaRepository);
        }
    }
}