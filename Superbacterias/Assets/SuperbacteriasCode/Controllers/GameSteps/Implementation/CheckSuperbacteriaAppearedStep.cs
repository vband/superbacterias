﻿using System.Threading.Tasks;
using DoubleDash.CodingTools.ReactiveVariables;
using SuperbacteriasCode.Controllers.GameSteps.Base;
using SuperbacteriasCode.Controllers.InstructionTexts;
using SuperbacteriasCode.Models;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.Implementation
{
    public class CheckSuperbacteriaAppearedStep : GameStep
    {
        [SerializeField] private DefeatController _defeatController;
        [SerializeField] private CardRepository _bacteriaRepository;
        [SerializeField] private IntReactiveScriptable _turnsSinceSuperbacteriaAppeared;
        [SerializeField] private GameSettings.GameSettings _gameSettings;
        [SerializeField] private EntryAnimationBehaviour _superBacteriaWarning;
        [SerializeField] private InstructionSequenceController _superBacteriaInstructionSequence;
        [SerializeField] private BoolReactiveScriptable _isGameActive;

        public override async Task<bool> RunStepLogic()
        {
            if (!_defeatController.ExistsSuperBacteria(_bacteriaRepository))
                return true;

            if (_turnsSinceSuperbacteriaAppeared.Value == 0)
            {
                await _superBacteriaWarning.PlayEntryAnimationAsync();
                await _superBacteriaInstructionSequence.PlaySequenceAsync();
            }
            else if (_turnsSinceSuperbacteriaAppeared.Value >= _gameSettings.TurnsAfterSuperBacteria)
            {
                _isGameActive.SetValue(false);
                _defeatController.SetSuperbacteriaDefeatReasonText();
                await _defeatController.ShowDefeatScreenAsync();
                return false;
            }

            _turnsSinceSuperbacteriaAppeared.SetValue(_turnsSinceSuperbacteriaAppeared.Value + 1);

            return true;
        }
    }
}