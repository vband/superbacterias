﻿using System.Threading.Tasks;
using SuperbacteriasCode.Controllers.GameSteps.Base;
using SuperbacteriasCode.Models;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.Implementation
{
    public class CheckTooManyBacteriasStep : GameStep
    {
        [SerializeField] private DefeatController _defeatController;
        [SerializeField] private CardRepository _bacteriaRepository;
        [SerializeField] private EntryAnimationBehaviour _tooManyBacteriasWarning;
        [SerializeField] private BoolReactiveScriptable _isGameActive;

        public override async Task<bool> RunStepLogic()
        {
            if (!_defeatController.TooManyBacterias(_bacteriaRepository))
                return true;
            
            await _tooManyBacteriasWarning.PlayEntryAnimationAsync();
            _isGameActive.SetValue(false);
            _defeatController.SetTooManyBacteriasDefeatReasonText();
            await _defeatController.ShowDefeatScreenAsync();

            return false;
        }
    }
}