﻿using System.Threading.Tasks;
using SuperbacteriasCode.Controllers.GameSteps.Base;
using SuperbacteriasCode.Utils;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.Implementation
{
    public class PlayGameStartAnimationStep : GameStep
    {
        [SerializeField] private Animator _gameStartWarningAnimator;
        [SerializeField] private string _gameStartAnimationState;
        
        public override async Task<bool> RunStepLogic()
        {
            await AnimatorTaskManager.PlayAsync(_gameStartWarningAnimator, _gameStartAnimationState);
            return true;
        }
    }
}