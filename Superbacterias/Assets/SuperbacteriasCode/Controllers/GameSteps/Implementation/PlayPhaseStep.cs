﻿using System.Threading.Tasks;
using SuperbacteriasCode.Controllers.GameSteps.Base;
using SuperbacteriasCode.Controllers.Phases;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.Implementation
{
    public class PlayPhaseStep : GameStep
    {
        [SerializeField] private PhaseController _phase;
        
        public override async Task<bool> RunStepLogic()
        {
            await _phase.PlayPhaseAsync();
            return true;
        }
    }
}