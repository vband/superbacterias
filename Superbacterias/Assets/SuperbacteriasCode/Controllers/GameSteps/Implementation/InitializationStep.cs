﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DoubleDash.CodingTools.ReactiveVariables;
using SuperbacteriasCode.Controllers.GameSteps.Base;
using SuperbacteriasCode.Models;
using SuperbacteriasCode.Models.Decks;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.Implementation
{
    public class InitializationStep : GameStep
    {
        [SerializeField] private BoolReactiveScriptable _isGameActive;
        [SerializeField] private List<Deck> _decksToBeInitialized = new List<Deck>();
        [SerializeField] private IntReactiveScriptable _turnCount;
        [SerializeField] private IntReactiveScriptable _turnsSinceSuperbacteriaAppeared;
        [SerializeField] private CardRepository _playerHandRepository;
        [SerializeField] private CardRepository _bacteriaRepository;
        

        public override Task<bool> RunStepLogic()
        {
            _isGameActive.SetValue(true);
            _decksToBeInitialized.ForEach(deck => deck.InitDeck());
            _turnCount.SetValue(0);
            _turnsSinceSuperbacteriaAppeared.SetValue(0);
            _playerHandRepository.Clear();
            _bacteriaRepository.Clear();


            return Task.Factory.StartNew(() => true);
        }
    }
}