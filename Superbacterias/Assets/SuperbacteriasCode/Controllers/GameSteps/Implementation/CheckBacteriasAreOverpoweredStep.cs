﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SuperbacteriasCode.Controllers.GameSteps.Base;
using SuperbacteriasCode.Controllers.InstructionTexts;
using SuperbacteriasCode.GameSettings;
using SuperbacteriasCode.Models;
using SuperbacteriasCode.Models.Cards;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.Implementation
{
    public class CheckBacteriasAreOverpoweredStep : GameStep
    {
        [SerializeField] private DeckDesignSettings _deckDesignSettings;
        [SerializeField] private CardRepository _bacteriaRepository;
        [SerializeField] private InstructionSequenceController _overpoweredBacteriasInstructionSequence;
        
        private bool _hasShownInstructionSequence;

        public override async Task<bool> RunStepLogic()
        {
            if (!AreBacteriasOverpowered())
                return true;

            if (!_hasShownInstructionSequence)
                await _overpoweredBacteriasInstructionSequence.PlaySequenceAsync();
            
            _hasShownInstructionSequence = true;

            return true;
        }
        
        private bool AreBacteriasOverpowered()
        {
            var bacteriasWithRevealedPower = GetBacteriasWithRevealedPower(_bacteriaRepository);
            var bacteriasTotalPower = GetBacteriasListTotalPower(bacteriasWithRevealedPower);
            var maxPowerThePlayerCanDefeatPerTurn = _deckDesignSettings.TreatmentMaxPower;

            return bacteriasTotalPower >= maxPowerThePlayerCanDefeatPerTurn * 2;
        }

        private static IEnumerable<BacteriaCard> GetBacteriasWithRevealedPower(CardRepository bacteriaRepository)
        {
            var bacteriasWithRevealedPower = new List<BacteriaCard>();
            
            for (var i = 0; i < bacteriaRepository.Count(); i++)
            {
                var card = bacteriaRepository.Get(i);
                if (card is BacteriaCard {IsPowerKnown: true} bacteria)
                {
                    bacteriasWithRevealedPower.Add(bacteria);
                }
            }

            return bacteriasWithRevealedPower;
        }

        private static int GetBacteriasListTotalPower(IEnumerable<BacteriaCard> bacterias)
        {
            return bacterias.Sum(bacteria => bacteria.Power);
        }
    }
}