﻿using System.Threading.Tasks;
using DoubleDash.CodingTools.ReactiveVariables;
using SuperbacteriasCode.Controllers.GameSteps.Base;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.Implementation
{
    public class IncrementTurnCountStep : GameStep
    {
        [SerializeField] private IntReactiveScriptable _turnCount;
        
        public override Task<bool> RunStepLogic()
        {
            _turnCount.SetValue(_turnCount.Value + 1);

            return Task.Factory.StartNew(() => true);
        }
    }
}