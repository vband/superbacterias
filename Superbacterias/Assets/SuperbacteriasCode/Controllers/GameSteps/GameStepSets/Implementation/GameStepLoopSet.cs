﻿using System.Threading.Tasks;
using SuperbacteriasCode.Controllers.GameSteps.GameStepSets.Base;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.GameStepSets.Implementation
{
    public class GameStepLoopSet : GameStepSet
    {
        [SerializeField] private BoolReactiveScriptable _loopWhileCondition;
        
        public override async Task<bool> RunStepLogic()
        {
            while (_loopWhileCondition != null && _loopWhileCondition.Value)
            {
                foreach (var step in _gameSteps)
                {
                    if (!await step.RunStepLogic())
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}