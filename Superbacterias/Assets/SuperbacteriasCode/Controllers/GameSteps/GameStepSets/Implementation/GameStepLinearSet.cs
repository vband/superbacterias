﻿using System.Threading.Tasks;
using SuperbacteriasCode.Controllers.GameSteps.GameStepSets.Base;

namespace SuperbacteriasCode.Controllers.GameSteps.GameStepSets.Implementation
{
    public class GameStepLinearSet : GameStepSet
    {
        public override async Task<bool> RunStepLogic()
        {
            foreach (var step in _gameSteps)
            {
                if (!await step.RunStepLogic())
                {
                    return false;
                }
            }

            return true;
        }
    }
}