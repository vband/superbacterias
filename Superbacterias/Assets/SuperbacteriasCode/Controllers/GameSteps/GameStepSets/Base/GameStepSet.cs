﻿using System.Collections.Generic;
using SuperbacteriasCode.Controllers.GameSteps.Base;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps.GameStepSets.Base
{
    public abstract class GameStepSet : GameStep
    {
        [SerializeField] protected List<GameStep> _gameSteps = new();
    }
}