﻿using System.Collections.Generic;
using SuperbacteriasCode.Controllers.GameSteps.Base;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.GameSteps
{
    public class GameStepsRunner : MonoBehaviour
    {
        [SerializeField] private List<GameStep> _steps;

        public async void RunSteps()
        {
            foreach (var step in _steps)
            {
                if (!await step.RunStepLogic())
                {
                    return;
                }
            }
        }
    }
}