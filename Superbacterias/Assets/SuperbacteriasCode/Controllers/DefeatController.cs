﻿using System.Linq;
using System.Threading.Tasks;
using SuperbacteriasCode.GameSettings;
using SuperbacteriasCode.Models;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Models.Mutations;
using SuperbacteriasCode.Utils;
using TMPro;
using UnityEngine;

namespace SuperbacteriasCode.Controllers
{
    public class DefeatController : EntryAnimationBehaviour
    {
        [SerializeField] private GameObject _defeatScreen;
        [SerializeField] private GameColors _gameColors;
        [SerializeField] private GameSettings.GameSettings _gameSettings;
        [SerializeField] private TextMeshProUGUI _defeatReasonText;

        public bool IsGameLost(CardRepository bacteriaRepository, out GameLostReason reason)
        {
            if (ExistsSuperBacteria(bacteriaRepository))
            {
                reason = GameLostReason.SuperBacteria;
                SetSuperbacteriaDefeatReasonText();
                return true;
            }

            if (TooManyBacterias(bacteriaRepository))
            {
                reason = GameLostReason.TooManyBacterias;
                SetTooManyBacteriasDefeatReasonText();
                return true;
            }

            reason = default;
            return false;
        }

        public void SetTooManyBacteriasDefeatReasonText()
        {
            _defeatReasonText.text = _gameSettings.TooManyBacteriasDefeatReasonText;
        }

        public void SetSuperbacteriaDefeatReasonText()
        {
            _defeatReasonText.text = _gameSettings.SuperbacteriaDefeatReasonText;
        }

        public bool TooManyBacterias(CardRepository bacteriaRepository)
        {
            var bacteriaCount = bacteriaRepository.Count(card => card is BacteriaCard);

            return bacteriaCount >= _gameSettings.MaxNumberOfBacterias;
        }

        public bool ExistsSuperBacteria(CardRepository bacteriaRepository)
        {
            for (var i = 0; i < bacteriaRepository.Count(); i++)
            {
                if (!(bacteriaRepository.Get(i) is BacteriaCard bacteria))
                    continue;

                if (IsSuperBacteria(bacteria))
                {
                    bacteria.RevealAll();
                    bacteria.InvokeOnModifiedCallbacks();
                    return true;
                }
            }

            return false;
        }

        public async Task ShowDefeatScreenAsync()
        {
            _defeatScreen.SetActive(true);

            await PlayEntryAnimationAsync();
        }

        private bool IsSuperBacteria(BacteriaCard bacteria)
        {
            var colorMutations = bacteria.Mutations.OfType<ColorBeneficialMutation>().ToList();

            if (colorMutations.Count + 1 < _gameColors.GetGameColors.Count)
                return false;

            var gameColors = ListHelper.CopyList(_gameColors.GetGameColors);

            foreach (var colorMutation in colorMutations)
            {
                gameColors.Remove(colorMutation.ColorAdded);
            }

            gameColors.Remove(bacteria.Color);

            return gameColors.Count == 0;
        }
    }
    
    public enum GameLostReason { SuperBacteria, TooManyBacterias }
}