﻿using System.Threading.Tasks;
using SuperbacteriasCode.Controllers.InstructionTexts;
using SuperbacteriasCode.GameSettings;
using SuperbacteriasCode.Models;
using SuperbacteriasCode.Models.Cards;
using UnityEngine;

namespace SuperbacteriasCode.Controllers
{
    public class BacteriasOverpowerInstructionBehaviour : MonoBehaviour
    {
        [SerializeField] private DeckDesignSettings _deckDesignSettings;
        [SerializeField] private CardRepository _bacteriaRepository;
        [SerializeField] private InstructionSequenceController _overpoweredBacteriasInstructionSequence;

        private bool _hasShownInstructionSequence;

        public async Task PerformBacteriasOverpoweredCheckAsync()
        {
            if (!BacteriasAreOverpowered())
                return;

            if (!_hasShownInstructionSequence)
                await _overpoweredBacteriasInstructionSequence.PlaySequenceAsync();
            
            _hasShownInstructionSequence = true;
        }

        private bool BacteriasAreOverpowered()
        {
            var bacteriasTotalPower = GetBacteriasTotalPower();
            var maxPowerThePlayerCanDefeatPerTurn = _deckDesignSettings.TreatmentMaxPower;

            return bacteriasTotalPower >= maxPowerThePlayerCanDefeatPerTurn * 2;
        }

        private int GetBacteriasTotalPower()
        {
            var totalPower = 0;

            for (var i = 0; i < _bacteriaRepository.Count(); i++)
            {
                totalPower += ((BacteriaCard) _bacteriaRepository.Get(i)).Power;
            }

            return totalPower;
        }
    }
}