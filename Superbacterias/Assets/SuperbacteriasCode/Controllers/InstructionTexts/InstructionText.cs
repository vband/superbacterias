﻿using System.Threading.Tasks;
using SuperbacteriasCode.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace SuperbacteriasCode.Controllers.InstructionTexts
{
    public class InstructionText : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private UnityEvent OnShow;
        [SerializeField] private UnityEvent OnHide;
        
        [SerializeField] private string _entryAnimation = "Entry";
        [SerializeField] private string _exitAnimation = "Exit";

        public async Task PlayEntryAnimationAsync()
        {
            OnShow.Invoke();
            
            await AnimatorTaskManager.PlayAsync(_animator, _entryAnimation);
        }

        public async Task PlayExitAnimationAsync()
        {
            OnHide.Invoke();
            
            await AnimatorTaskManager.PlayAsync(_animator, _exitAnimation);
        }
    }
}