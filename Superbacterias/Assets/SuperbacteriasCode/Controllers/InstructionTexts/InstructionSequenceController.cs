﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SuperbacteriasCode.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace SuperbacteriasCode.Controllers.InstructionTexts
{
    public class InstructionSequenceController : MonoBehaviour
    {
        [SerializeField] private BoolReactiveScriptable _context;
        [SerializeField] private Animator _exclamation;
        [SerializeField] private List<InstructionText> _instructionTexts = new List<InstructionText>();
        [SerializeField] private string _entryAnimation = "Entry";

        [SerializeField] private UnityEvent OnStartWaiting;
        [SerializeField] private UnityEvent OnFinishWaiting;
        [SerializeField] private UnityEvent OnStartPlayingInstructionTexts;
        [SerializeField] private UnityEvent OnFinishPlayingInstructionTexts;

        private bool _waiting;

        public async void PlaySequence()
        {
            await PlaySequenceAsync();
        }

        public async Task PlaySequenceAsync()
        {
            if (_context != null && _context.Value == false)
                return;
            
            OnStartPlayingInstructionTexts.Invoke();
            
            await AnimatorTaskManager.PlayAsync(_exclamation, _entryAnimation);

            foreach (var instructionText in _instructionTexts)
            {
                await instructionText.PlayEntryAnimationAsync();

                OnStartWaiting.Invoke();
                await WaitSkip();
                OnFinishWaiting.Invoke();

                await instructionText.PlayExitAnimationAsync();
            }
            
            OnFinishPlayingInstructionTexts.Invoke();
        }

        private async Task WaitSkip()
        {
            _waiting = true;

            while (_waiting)
            {
                await Task.Delay(1);
            }
        }

        public void OnSkip()
        {
            _waiting = false;
        }
    }
}