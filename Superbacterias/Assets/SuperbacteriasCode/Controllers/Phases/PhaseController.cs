﻿using System.Threading.Tasks;
using DoubleDash.CodingTools.ReactiveVariables;
using SuperbacteriasCode.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace SuperbacteriasCode.Controllers.Phases
{
    public abstract class PhaseController : EntryAnimationBehaviour
    {
        [SerializeField] protected StringReactiveScriptable _currentPhase;
        [SerializeField] protected string _thisPhase;
        [SerializeField] protected BoolReactiveScriptable _isPhasePaused;

        [SerializeField] protected UnityEvent OnPhaseStart;

        public virtual Task PlayPhaseAsync()
        {
            _currentPhase.SetValue(_thisPhase);
            return Task.CompletedTask;
        }

        protected async Task WaitWhilePhaseIsPaused()
        {
            while (_isPhasePaused != null && _isPhasePaused.Value)
            {
                await Task.Delay(1);
            }
        }
    }
}