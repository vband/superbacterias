﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DoubleDash.CodingTools.ReactiveVariables;
using SuperbacteriasCode.Models;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Models.Decks;
using SuperbacteriasCode.Views.CardSets;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.Phases
{
    public class BuyPhaseController : WaitPlayerChooseCardBehaviour
    {
        [SerializeField] private GameSettings.GameSettings _gameSettings;
        [SerializeField] private IntReactiveScriptable _turnCount;
        [SerializeField] private Deck _treatmentDeck;
        [SerializeField] private GameObject _drawCardsScreen;
        [SerializeField] private CardSetView _drawCardSetView;
        [SerializeField] private CardRepository _playerCardRepository;
        [SerializeField] private CardRepository _bacteriaRepository;
        [SerializeField] private bool _allowConsecutiveSurgeCards;
        
        public override async Task PlayPhaseAsync()
        {
            await base.PlayPhaseAsync();
            
            await PlayEntryAnimationAsync();
            
            OnPhaseStart.Invoke();

            await WaitWhilePhaseIsPaused();
            
            SetDrawCardsScreenActive(true);
            
            _gameSettings.GetNumberOfCardsShownAndChosen(_turnCount.Value, out var cardsShownCount, out var cardsChosenCount);

            // Puxa as cartas do baralho
            var cardsShownList = new List<Card>();
            for (var i = 0; i < cardsShownCount; i++)
            {
                var card = TakeCardFromTreatmentDeckTop();

                if (!card.ShouldBeDrawnFromDeckNow(_turnCount, _gameSettings))
                {
                    InsertCardIntoTreatmentDeckBottom(card);
                    i--;
                    continue;
                }
                
                cardsShownList.Add(card);
                UpdateScreen(cardsShownList);
                var (item1, isSurge) = await card.OnDrawFromDeck(_bacteriaRepository, cardsShownList, i);
                i = item1;
                await WaitWhilePhaseIsPaused();

                if (isSurge)
                    CheckForConsecutiveSurgeCards();
            }
            
            UpdateScreen(cardsShownList);
            
            // Deixa o jogador escolher suas cartas
            for (var i = 0; i < cardsChosenCount; i++)
            {
                var chosenCard = await WaitPlayerChooseCard();

                _playerCardRepository.Add(chosenCard);
                cardsShownList.Remove(chosenCard);
                
                UpdateScreen(cardsShownList);
            }
            
            // Devolve as cartas não escolhidas para o baralho
            cardsShownList.ForEach(_treatmentDeck.InsertToBottom);
            
            SetDrawCardsScreenActive(false);
        }

        private void CheckForConsecutiveSurgeCards()
        {
            while (true)
            {
                if (!_allowConsecutiveSurgeCards
                    && _treatmentDeck.TryLookAtTop(out var topCard)
                    && topCard is SurgeCard)
                {
                    var card = TakeCardFromTreatmentDeckTop();
                    InsertCardIntoTreatmentDeckBottom(card);

                    continue;
                }

                break;
            }
        }

        private Card TakeCardFromTreatmentDeckTop()
        {
            return _treatmentDeck.TakeFromTop();
        }

        private void InsertCardIntoTreatmentDeckBottom(Card card)
        {
            _treatmentDeck.InsertToBottom(card);
        }

        private void SetDrawCardsScreenActive(bool active)
        {
            if (!_drawCardsScreen || !_drawCardsScreen.gameObject)
                return;
            
            _drawCardsScreen.gameObject.SetActive(active);
        }

        private void UpdateScreen(List<Card> cards)
        {
            _drawCardSetView.UpdateView(cards);
        }
    }
}