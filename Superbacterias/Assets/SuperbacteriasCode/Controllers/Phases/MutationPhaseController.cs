﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DoubleDash.CodingTools.ReactiveVariables;
using SuperbacteriasCode.GameSettings;
using SuperbacteriasCode.Models;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Models.Mutations;
using SuperbacteriasCode.Views;
using SuperbacteriasCode.Views.Cards;
using SuperbacteriasCode.Views.CardSets;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.Phases
{
    public class MutationPhaseController : PhaseController
    {
        [SerializeField] private CardRepository _bacteriaRepository;
        [SerializeField] private CardSetView _bacteriasView;
        [SerializeField] private GameSettings.GameSettings _gameSettings;
        [SerializeField] private GameColors _gameColors;
        [SerializeField] private IntReactiveScriptable _turnCount;

        private bool _forcedMutation;
        
        public override async Task PlayPhaseAsync()
        {
            await base.PlayPhaseAsync();
            
            if (_turnCount.Value < _gameSettings.MinTurnsForMutation)
                return;
            
            await PlayEntryAnimationAsync();
            
            OnPhaseStart.Invoke();
            
            await WaitWhilePhaseIsPaused();

            await RegisterPossibleNewMutationsAsync();
        }

        private async Task RegisterPossibleNewMutationsAsync()
        {
            for (var i = 0; i < _bacteriaRepository.Count(); i++)
            {
                var bacteria = _bacteriaRepository.Get(i) as BacteriaCard;

                if (bacteria is null)
                    continue;

                if (!bacteria.Newborn)
                    continue;

                bacteria.Newborn = false;

                if (!ShouldMutateThisTurn())
                    continue;

                if (!TryCreateRandomMutation(bacteria, out var mutation))
                    continue;

                await mutation.RegisterMutationAsync(bacteria,
                    _bacteriasView.Get(_bacteriaRepository.IndexOf(bacteria)) as BacteriaCardView);
                
                _bacteriaRepository.NotifyRepositoryModified();
            }
        }

        private bool ShouldMutateThisTurn()
        {
            if (_turnCount.Value == _gameSettings.ForcedMutationTurn
                && !_forcedMutation)
            {
                _forcedMutation = true;
                return true;
            }
            
            return Random.value <= _gameSettings.MutationChance;
        }

        private bool TryCreateRandomMutation(BacteriaCard bacteria, out Mutation mutation)
        {
            if (ShouldCreatePrejudicialMutation() && TryCreatePrejudicialMutation(bacteria, out var prejudicialMutation))
            {
                mutation = prejudicialMutation;
                return true;
            }

            if (ShouldCreateNeutralMutation() && TryCreateNeutralMutation(bacteria, out var neutralMutation))
            {
                mutation = neutralMutation;
                return true;
            }

            if (ShouldCreatePowerBeneficialMutation() && TryCreatePowerBeneficialMutation(bacteria, out var powerBeneficialMutation))
            {
                mutation = powerBeneficialMutation;
                return true;
            }

            if (TryCreateColorBeneficialMutation(bacteria, out var colorBeneficialMutation))
            {
                mutation = colorBeneficialMutation;
                return true;
            }

            mutation = null;
            return false;
        }

        private bool ShouldCreatePrejudicialMutation()
        {
            return Random.value <= _gameSettings.PrejudicialMutationChance;
        }

        private bool ShouldCreateNeutralMutation()
        {
            return Random.value <= _gameSettings.NeutralMutationChance;
        }

        private bool ShouldCreatePowerBeneficialMutation()
        {
            return Random.value <= _gameSettings.PowerBeneficialMutationChance;
        }

        private bool TryCreatePrejudicialMutation(BacteriaCard bacteria, out PrejudicialMutation prejudicialMutation)
        {
            if (bacteria.Mutations.Exists(mutation =>
                    mutation is PrejudicialMutation || mutation is PowerBeneficialMutation))
            {
                prejudicialMutation = null;
                return false;
            }
            
            var min = _gameSettings.MinPowerPrejudicialMutationValue;
            var max = _gameSettings.MaxPowerPrejudicialMutationValue;
            
            prejudicialMutation = new PrejudicialMutation(Random.Range(min, max + 1));
            return true;
        }

        private bool TryCreateNeutralMutation(BacteriaCard bacteria, out NeutralMutation neutralMutation)
        {
            if (bacteria.Mutations.Exists(mutation => mutation is NeutralMutation))
            {
                neutralMutation = null;
                return false;
            }
            
            neutralMutation = new NeutralMutation();
            return true;
        }

        private bool TryCreateColorBeneficialMutation(BacteriaCard bacteria, out ColorBeneficialMutation colorBeneficialMutation)
        {
            var existingColors = new List<Models.Color>();
            existingColors.Add(bacteria.Color);
            
            existingColors.AddRange(from mutation in bacteria.Mutations
                .Where(mutation => mutation is ColorBeneficialMutation) 
                select mutation as ColorBeneficialMutation into colorMutation 
                where colorMutation is { } 
                select colorMutation.ColorAdded);

            var newColor = _gameColors.GetRandomColorExcept(existingColors);

            if (newColor is null)
            {
                colorBeneficialMutation = null;
                return false;
            }
            
            colorBeneficialMutation = new ColorBeneficialMutation(newColor);
            return true;
        }

        private bool TryCreatePowerBeneficialMutation(BacteriaCard bacteria, out PowerBeneficialMutation powerBeneficialMutation)
        {
            if (bacteria.Mutations.Exists(mutation =>
                    mutation is PrejudicialMutation || mutation is PowerBeneficialMutation))
            {
                powerBeneficialMutation = null;
                return false;
            }
            
            var min = _gameSettings.MinPowerBeneficialMutationValue;
            var max = _gameSettings.MaxPowerBeneficialMutationValue;
            
            powerBeneficialMutation = new PowerBeneficialMutation(Random.Range(min, max + 1));
            return true;
        }
    }
}