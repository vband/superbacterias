﻿using System.Threading.Tasks;
using SuperbacteriasCode.Models;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Utils;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.Phases
{
    public class ReproductionPhaseController : PhaseController
    {
        [SerializeField] private CardRepository _bacteriaRepository;
        
        public override async Task PlayPhaseAsync()
        {
            await base.PlayPhaseAsync();
            
            await PlayEntryAnimationAsync();
            
            OnPhaseStart.Invoke();
            
            await WaitWhilePhaseIsPaused();

            var count = _bacteriaRepository.Count();
            for (var i = count - 1; i >= 0; i--)
            {
                var parent = _bacteriaRepository.Get(i) as BacteriaCard;
                if (parent is null) continue;

                var child = parent.ProduceChild();
                _bacteriaRepository.Insert(i + 1, child);

                //TODO: Animações
                await Task.Delay(500);
            }
        }
    }
}