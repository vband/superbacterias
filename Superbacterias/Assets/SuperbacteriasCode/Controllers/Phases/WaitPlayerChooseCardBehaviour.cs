using System.Threading.Tasks;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Utils;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.Phases
{
    public abstract class WaitPlayerChooseCardBehaviour : PhaseController
    {
        private bool _hasPlayerChosenCard;
        private Card _chosenCard;
        
        public void OnPlayerChoseCard(Card card)
        {
            _hasPlayerChosenCard = true;
            _chosenCard = card;
        }

        protected async Task<Card> WaitPlayerChooseCard()
        {
            _hasPlayerChosenCard = false;
            await TaskHelper.WaitUntil(() => _hasPlayerChosenCard);
            return _chosenCard;
        }
    }
}
