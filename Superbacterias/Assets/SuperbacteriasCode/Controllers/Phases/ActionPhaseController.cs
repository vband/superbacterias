using System.Threading.Tasks;
using DoubleDash.CodingTools.ReactiveVariables;
using SuperbacteriasCode.Models;
using SuperbacteriasCode.Models.Cards;
using UnityEngine;
using UnityEngine.Events;

namespace SuperbacteriasCode.Controllers.Phases
{
    public class ActionPhaseController : WaitPlayerChooseCardBehaviour
    {
        [SerializeField] private CardRepository _bacteriaRepository;
        [SerializeField] private CardRepository _playerCards;
        [SerializeField] private IntReactiveScriptable _killedBacteriasCount;
        [SerializeField] private IntReactiveScriptable _treatmentPowerUsedCount;

        [SerializeField] private UnityEvent _onBacteriaColorRevealed;

        public override async Task PlayPhaseAsync()
        {
            var initialBacteriasCount = _bacteriaRepository.Count();
            
            await base.PlayPhaseAsync();
            
            await PlayEntryAnimationAsync();
            
            OnPhaseStart.Invoke();

            await WaitWhilePhaseIsPaused();

            var chosenCard = await WaitPlayerChooseCard();

            if (chosenCard is TreatmentCard treatmentCard)
                _treatmentPowerUsedCount.SetValue(_treatmentPowerUsedCount.Value + treatmentCard.Power);

            var bacteriasWithColorKnownBeforeCount = _bacteriaRepository.Count(card => card is BacteriaCard {IsColorKnown: true});
            
            await chosenCard.UseOnBacterias(_bacteriaRepository);
            
            var bacteriasWithColorKnownAfterCount = _bacteriaRepository.Count(card => card is BacteriaCard {IsColorKnown: true});
            
            CleanRepositoryOfDeadCards(_bacteriaRepository);
            CleanRepositoryOfDeadCards(_playerCards);
            
            _bacteriaRepository.NotifyRepositoryModified();
            _playerCards.NotifyRepositoryModified();

            var finalBacteriasCount = _bacteriaRepository.Count();
            var bacteriasKilledThisPhase = initialBacteriasCount - finalBacteriasCount;

            if (bacteriasKilledThisPhase > 0)
                _killedBacteriasCount.SetValue(_killedBacteriasCount.Value + bacteriasKilledThisPhase);

            if (bacteriasWithColorKnownAfterCount <= bacteriasWithColorKnownBeforeCount)
                return;
            
            _onBacteriaColorRevealed.Invoke();
            await WaitWhilePhaseIsPaused();
        }

        private static void CleanRepositoryOfDeadCards(CardRepository repository)
        {
            var count = repository.Count();
            for (var i = count - 1; i >= 0; i--)
            {
                var combatant = repository.Get(i) as CombatantCard;
                
                if (combatant is null)
                    continue;
                
                if (combatant.Power <= 0)
                    repository.RemoveAt(i);
            }
        }
    }
}
