﻿using SuperbacteriasCode.Models;
using SuperbacteriasCode.Models.Decks;
using UnityEngine;

namespace SuperbacteriasCode.Controllers
{
    public class BacteriaGenerator : MonoBehaviour
    {
        [SerializeField] private Deck _bacteriaDeck;
        [SerializeField] private GameSettings.GameSettings _gameSettings;
        [SerializeField] private CardRepository _bacteriaRepository;

        public void InitializeBacterias()
        {
            for (var i = 0; i < _gameSettings.BacteriasInitialNumber; i++)
            {
                DrawNewBacteria();
            }
        }
        
        private void DrawNewBacteria()
        {
            var bacteriaCard = _bacteriaDeck.TakeFromTop();
            _bacteriaRepository.Add(bacteriaCard);
        }
    }
}