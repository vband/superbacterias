﻿using System.Threading.Tasks;
using SuperbacteriasCode.Utils;
using UnityEngine;

namespace SuperbacteriasCode.Controllers
{
    public class EntryAnimationBehaviour : MonoBehaviour
    {
        [SerializeField] protected Animator _animator;
        [SerializeField] protected string _entryAnimationState;
        
        public async Task PlayEntryAnimationAsync()
        {
            await AnimatorTaskManager.PlayAsync(_animator, _entryAnimationState);
        }
    }
}