using SuperbacteriasCode.Views;
using SuperbacteriasCode.Views.Cards;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.CardBehaviours
{
    public abstract class CardBehaviour : MonoBehaviour
    {
        [SerializeField] protected CardView _cardView;
    }
}
