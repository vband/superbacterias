﻿using SuperbacteriasCode.Utils.UnityEventImplementations;
using UnityEngine;

namespace SuperbacteriasCode.Controllers.CardBehaviours
{
    public class ClickableCardBehaviour : CardBehaviour
    {
        [SerializeField] private UnityEventCard _onPlayerChoseThis;
        
        public void OnPlayerChoseThis()
        {
            _onPlayerChoseThis.Invoke(_cardView.Card);
        }
    }
}