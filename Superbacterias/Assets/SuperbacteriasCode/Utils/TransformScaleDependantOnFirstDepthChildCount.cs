﻿using System;
using UnityEngine;

namespace SuperbacteriasCode.Utils
{
    public class TransformScaleDependantOnFirstDepthChildCount : MonoBehaviour
    {
        [SerializeField] private Transform _transformToScale;
        [SerializeField] private Transform _transformParent;
        [SerializeField] private int _childCountThreshold;
        [SerializeField] private ScaleOperation _scaleOperation;
        [SerializeField] private Vector3 _baseScale;
        [SerializeField] private int _checkEveryNFrames;

        private int _frameCounter;

        private void Update()
        {
            if (_checkEveryNFrames <= 0)
                return;

            _frameCounter++;

            if (_frameCounter % _checkEveryNFrames != 0)
                return;
            
            UpdateScale(_transformToScale, GetFirstDepthChildCount(_transformParent), _childCountThreshold, _scaleOperation, _baseScale);
            _frameCounter = 0;
        }

        private static void UpdateScale(Transform transformToScale, int childCount, int threshold, ScaleOperation operation, Vector3 baseScale)
        {
            Vector3 finalScale;

            if (childCount <= threshold)
            {
                finalScale = baseScale;
            }
            else
            {
                float scaleValue;
                switch (operation)
                {
                    case ScaleOperation.IncreaseScale:
                        scaleValue = (float) childCount / threshold;
                        finalScale = new Vector3(scaleValue, scaleValue, scaleValue);
                        break;

                    case ScaleOperation.DecreaseScale:
                        scaleValue = (float) threshold / childCount;
                        finalScale = new Vector3(scaleValue, scaleValue, scaleValue);
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(nameof(operation), operation, "Unexpected value.");
                }
            }

            transformToScale.localScale = finalScale;
        }

        private static int GetFirstDepthChildCount(Transform transformParent)
        {
            var firstDepthChildCount = 0;

            for (var i = 0; i < transformParent.childCount; i++)
            {
                var child = transformParent.GetChild(i);
                
                if (IsImmediateChildOf(child, transformParent))
                    firstDepthChildCount++;
            }

            return firstDepthChildCount;
        }

        private static bool IsImmediateChildOf(Transform child, Transform transformParent)
        {
            return child.parent.Equals(transformParent);
        }

        [Serializable]
        private enum ScaleOperation {IncreaseScale, DecreaseScale}
    }
}