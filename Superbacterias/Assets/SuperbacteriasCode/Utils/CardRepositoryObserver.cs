﻿using System.Collections.Generic;
using SuperbacteriasCode.Models;
using SuperbacteriasCode.Models.Cards;
using UnityEngine;
using UnityEngine.Events;

namespace SuperbacteriasCode.Utils
{
    public class CardRepositoryObserver : MonoBehaviour
    {
        [SerializeField] private CardRepository _cardRepository;
        [SerializeField] private UnityEvent<List<Card>> _onRepositoryChanged;
        
        private void OnEnable()
        {
            if (!_cardRepository)
                return;
            
            _cardRepository.OnModifyRepository += OnRepositoryChanged;
        }

        private void OnDisable()
        {
            if (!_cardRepository)
                return;

            _cardRepository.OnModifyRepository -= OnRepositoryChanged;
        }

        private void OnRepositoryChanged(List<Card> cards)
        {
            _onRepositoryChanged.Invoke(cards);
        }
    }
}