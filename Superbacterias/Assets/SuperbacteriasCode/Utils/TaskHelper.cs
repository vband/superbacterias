﻿using System;
using System.Threading.Tasks;

namespace SuperbacteriasCode.Utils
{
    public class TaskHelper
    {
        public static async Task WaitWhile(Func<bool> condition)
        {
            while (condition())
            {
                await Task.Delay(1);
            }
        }

        public static async Task WaitUntil(Func<bool> condition)
        {
            while (!condition())
            {
                await Task.Delay(1);
            }
        }
    }
}