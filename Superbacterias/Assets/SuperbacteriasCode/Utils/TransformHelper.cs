using System.Collections.Generic;
using UnityEngine;

namespace SuperbacteriasCode.Utils
{
    public class TransformHelper
    {
        public static void DestroyChildren(Transform trans)
        {
            var children = GetChildren(trans);
            
            DestroyListOfChildren(children);
        }

        private static void DestroyListOfChildren(IReadOnlyList<Transform> children)
        {
            for (var i = children.Count - 1; i >= 0; i--)
            {
                Object.DestroyImmediate(children[i].gameObject);
            }
        }

        public static void DestroyChildrenExcept(Transform trans, Transform exception)
        {
            var children = GetChildren(trans);

            if (children.Contains(exception))
                children.Remove(exception);

            DestroyListOfChildren(children);
        }

        public static List<Transform> GetChildren(Transform trans)
        {
            var children = new List<Transform>();

            if (!trans)
                return children;
            
            for (var i = 0; i < trans.childCount; i++)
            {
                children.Add(trans.GetChild(i));
            }

            return children;
        }
    }
}
