﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SuperbacteriasCode.Utils
{
    public class SerializedUnityEvents : MonoBehaviour
    {
        [SerializeField] private List<SerializedUnityEvent> _events;

        public void InvokeEvent(string eventName)
        {
            var searchIndex = _events.FindIndex(element => element.EventName.Equals(eventName));

            if (searchIndex < 0)
            {
                Debug.LogError($"Unable to invoke event named {eventName}.");
                return;
            }
            
            _events[searchIndex].Event.Invoke();
        }

        [System.Serializable]
        public class SerializedUnityEvent
        {
            public string EventName;
            public UnityEvent Event;
        }
    }
}