using UnityEngine;
using UnityEngine.Events;

namespace SuperbacteriasCode.Utils
{
    public class MouseButtonDownHandler : MonoBehaviour
    {
        public UnityEvent OnClick;
    
        private void Update()
        {
            TryInvokeOnClickEvent();
        }

        private void TryInvokeOnClickEvent()
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnClick.Invoke();
            }
        }
    }
}
