﻿using UnityEngine;
using UnityEngine.Events;

namespace SuperbacteriasCode.Utils
{
    public class ConditionalBoolReactiveEvents : MonoBehaviour
    {
        [SerializeField] private BoolReactiveScriptable _boolReactiveScriptable;

        [SerializeField] private UnityEvent InvokeIfTrue;
        [SerializeField] private UnityEvent InvokeIfFalse;

        public void InvokeEvents()
        {
            if (!_boolReactiveScriptable)
            {
                Debug.LogError("Unable to invoke conditional bool reactive events because the reactive scriptable is null");
                return;
            }
            
            if (_boolReactiveScriptable.Value)
                InvokeIfTrue.Invoke();
            else
                InvokeIfFalse.Invoke();
        }
    }
}