using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace SuperbacteriasCode.Utils
{
    public class PointerClickHandler : MonoBehaviour, IPointerClickHandler
    {
        public UnityEvent OnClick;
    
        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick.Invoke();
        }
    }
}
