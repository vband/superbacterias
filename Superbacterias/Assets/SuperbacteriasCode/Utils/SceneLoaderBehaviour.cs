﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace SuperbacteriasCode.Utils
{
    public class SceneLoaderBehaviour : MonoBehaviour
    {
        public void ReloadScene()
        {
            var activeScene = SceneManager.GetActiveScene();
            LoadScene(activeScene.name);
        }

        public void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}