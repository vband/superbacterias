﻿using System.Threading.Tasks;
using UnityEngine;

namespace SuperbacteriasCode.Utils
{
    public class AnimatorTaskManager
    {
        public static async Task PlayAsync(Animator animator, string stateName)
        {
            animator.Play(stateName);
            await WaitFinishState(animator, stateName);
        }

        private static async Task WaitFinishState(Animator animator, string stateName)
        {
            if (!IsPlaying(animator, stateName))
                await TaskHelper.WaitUntil(() => IsPlaying(animator, stateName));

            await TaskHelper.WaitWhile(() =>
                IsPlaying(animator, stateName) && GetCurrentStateNormalizedTime(animator) < 1);
        }

        private static bool IsPlaying(Animator animator, string stateName)
        {
            try
            {
                return animator.GetCurrentAnimatorStateInfo(0).IsName(stateName);
            }
            catch (MissingReferenceException)
            {
                return false;
            }
        }

        private static float GetCurrentStateNormalizedTime(Animator animator)
        {
            return animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        }
    }
}