﻿using System.Collections.Generic;
using DoubleDash.CodingTools.ReactiveVariables;
using UnityEngine;

namespace SuperbacteriasCode.Utils.PlayerPrefs
{
    public class DeletePlayerPrefsSerializedKeys : MonoBehaviour
    {
        [SerializeField] private List<StringReactiveScriptable> _keys;

        public void DeleteKeys()
        {
            foreach (var key in _keys)
            {
                UnityEngine.PlayerPrefs.DeleteKey(key.Value);
            }
        }
    }
}