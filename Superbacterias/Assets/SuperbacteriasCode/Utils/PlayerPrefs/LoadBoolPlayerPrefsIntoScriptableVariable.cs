﻿using System.Collections.Generic;
using DoubleDash.CodingTools.ReactiveVariables;
using UnityEngine;

namespace SuperbacteriasCode.Utils.PlayerPrefs
{
    public class LoadBoolPlayerPrefsIntoScriptableVariable : MonoBehaviour
    {
        [SerializeField] private List<PlayerPrefsKeyAndBoolScriptableVariablePair> _playerPrefsKeyAndBoolScriptableVariablePairs;

        public void LoadPlayerPrefsIntroScriptables()
        {
            foreach (var pair in _playerPrefsKeyAndBoolScriptableVariablePairs)
            {
                if (!TryGetPlayerPrefsSavedValue(pair, out var boolValue)) continue;
                
                pair.BoolReactiveScriptable.SetValue(boolValue);
            }
        }

        private static bool TryGetPlayerPrefsSavedValue(PlayerPrefsKeyAndBoolScriptableVariablePair pair, out bool boolValue)
        {
            boolValue = false;
            
            if (!UnityEngine.PlayerPrefs.HasKey(pair.PlayerPrefsKey.Value)) return false;

            var playerPrefsIntValue = UnityEngine.PlayerPrefs.GetInt(pair.PlayerPrefsKey.Value);
            boolValue = playerPrefsIntValue == 1;
            return true;
        }

        [System.Serializable]
        public class PlayerPrefsKeyAndBoolScriptableVariablePair
        {
            public StringReactiveScriptable PlayerPrefsKey;
            public BoolReactiveScriptable BoolReactiveScriptable;
        }
    }
}