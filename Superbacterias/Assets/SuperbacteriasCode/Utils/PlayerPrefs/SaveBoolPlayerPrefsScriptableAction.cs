﻿using DoubleDash.CodingTools.ReactiveVariables;
using UnityEngine;

namespace SuperbacteriasCode.Utils.PlayerPrefs
{
    [CreateAssetMenu(fileName = "SaveBoolPlayerPrefsScriptableAction", menuName = "Superbacterias/ScriptableActions/PlayerPrefs/SaveBool")]
    public class SaveBoolPlayerPrefsScriptableAction : ScriptableObject
    {
        [SerializeField] private StringReactiveScriptable _key;
        [SerializeField] private bool _invertSignal;

        public void SaveBool(bool value)
        {
            var boolValue = _invertSignal ? !value : value;
            var intValue = boolValue ? 1 : 0;

            UnityEngine.PlayerPrefs.SetInt(_key.Value, intValue);
        }
    }
}