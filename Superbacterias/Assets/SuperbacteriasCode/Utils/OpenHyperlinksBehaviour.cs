using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SuperbacteriasCode.Utils
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class OpenHyperlinksBehaviour : MonoBehaviour, IPointerClickHandler
    {
        private TextMeshProUGUI _textMeshPro;
        [SerializeField] private Camera _camera;
    
        private void Start()
        {
            _textMeshPro = GetComponent<TextMeshProUGUI>();
        }

        public void OnPointerClick(PointerEventData eventData) {
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(_textMeshPro, Input.mousePosition, _camera);
        
            if( linkIndex != -1 ) { // was a link clicked?
                TMP_LinkInfo linkInfo = _textMeshPro.textInfo.linkInfo[linkIndex];

                // open the link id as a url, which is the metadata we added in the text field
                Application.OpenURL(linkInfo.GetLinkID());
            }
        }
    }
}
