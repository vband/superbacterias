﻿using System;
using System.Collections.Generic;

namespace SuperbacteriasCode.Utils
{
    public static class ListExtension
    {
        private static readonly Random Rng = new Random();
        
        public static void ShuffleList<T>(this IList<T> list)
        {
            var n = list.Count;

            while (n > 1)
            {
                n--;
                var k = Rng.Next(n + 1);
                (list[n], list[k]) = (list[k], list[n]);
            }
        }
    }
}