﻿using SuperbacteriasCode.Models.Cards;
using UnityEngine.Events;

namespace SuperbacteriasCode.Utils.UnityEventImplementations
{
    [System.Serializable]
    public class UnityEventCard : UnityEvent<Card>
    {
        
    }
}