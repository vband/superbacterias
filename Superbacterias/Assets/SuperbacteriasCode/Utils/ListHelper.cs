﻿using System.Collections.Generic;

namespace SuperbacteriasCode.Utils
{
    public class ListHelper
    {
        public static List<T> CopyList<T>(List<T> list)
        {
            List<T> returnList = new List<T>();
            returnList.AddRange(list);
            return returnList;
        }
    }
}
