﻿using DoubleDash.CodingTools.GameEvents;
using UnityEngine;

namespace SuperbacteriasCode.Utils.GameEventImplementations
{
    [CreateAssetMenu(fileName = "BoolScriptableGameEvent", menuName = "Superbacterias/GameEvents/BoolScriptableGameEvent")]
    public class BoolScriptableGameEvent : ScriptableGameEvent<bool>
    {
        public override IGameEvent<DelegateEventSubscriber<bool>> Value { get => _event; set => _event = (MultiStepGameEvent<bool>) value; }
    }
}