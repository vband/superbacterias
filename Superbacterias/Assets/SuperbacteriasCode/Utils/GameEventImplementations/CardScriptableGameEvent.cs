﻿using DoubleDash.CodingTools.GameEvents;
using SuperbacteriasCode.Models.Cards;
using UnityEngine;

namespace SuperbacteriasCode.Utils.GameEventImplementations
{
    [CreateAssetMenu(fileName = "CardScriptableGameEvent", menuName = "Superbacterias/GameEvents/CardScriptableGameEvent")]
    public class CardScriptableGameEvent : ScriptableGameEvent<Card>
    {
        public override IGameEvent<DelegateEventSubscriber<Card>> Value { get => _event; set => _event = (MultiStepGameEvent<Card>) value; }
    }
}