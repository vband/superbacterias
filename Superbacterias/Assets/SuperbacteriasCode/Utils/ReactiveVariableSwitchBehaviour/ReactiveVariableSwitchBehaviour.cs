﻿using System.Collections.Generic;
using DoubleDash.CodingTools.ReactiveVariables;
using UnityEngine;
using UnityEngine.Events;

namespace SuperbacteriasCode.Utils.ReactiveVariableSwitchBehaviour
{
    public abstract class ReactiveVariableSwitchBehaviour<Type> : MonoBehaviour
    {
        [SerializeField] protected ReactiveVariableScriptable<Type> _reactiveVariable;
        [SerializeField] protected List<ReactiveVariableSwitchCase<Type>> _switch;

        protected void OnEnable()
        {
            _reactiveVariable.Subscribe(OnValueChange);
            _reactiveVariable.OnValueChangedEvent.Trigger();
        }
        
        private void OnValueChange<T>(T value)
        {
            var searchIndex = _switch.FindIndex(switchCase => switchCase.Value.Equals(value));

            for (var i = 0; i < _switch.Count; i++)
            {
                if (searchIndex == i)
                    _switch[i].IfEqual.Invoke(_switch[i].Value);
                else
                    _switch[i].IfNotEqual.Invoke(_switch[i].Value);
            }
        }
    }

    [System.Serializable]
    public class ReactiveVariableSwitchCase<Type>
    {
        public Type Value;
        public UnityEvent<Type> IfEqual;
        public UnityEvent<Type> IfNotEqual;
    }
}