﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SuperbacteriasCode.Utils
{
    public class ScrollObserver : MonoBehaviour
    {
        [System.Serializable] public enum ScrollBarType {Horizontal, Vertical}

        [SerializeField] private ScrollRect _scrollRect;
        [SerializeField] private ScrollBarType _scrollBarType;
        [SerializeField] private bool _invokeEventsOnStart;
        
        [SerializeField] private UnityEvent<int> OnCurrentPageChanged;
        [SerializeField] private UnityEvent<int> OnTotalPagesChanged;

        private Scrollbar _scrollbar;

        private void Start()
        {
            _scrollbar = _scrollBarType == ScrollBarType.Horizontal
                ? _scrollRect.horizontalScrollbar
                : _scrollRect.verticalScrollbar;
            
            _scrollbar.onValueChanged.AddListener(OnScrollbarValueChanged);
            
            if (!_invokeEventsOnStart)
                return;
            
            OnScrollbarValueChanged(_scrollbar.value);
        }

        private void OnScrollbarValueChanged(float value)
        {
            var currentPage = GetCurrentPage(_scrollbar);
            var totalPages = GetTotalPages(_scrollbar);
            
            OnCurrentPageChanged.Invoke(currentPage);
            OnTotalPagesChanged.Invoke(totalPages);
        }

        private static int GetCurrentPage(Scrollbar scrollbar)
        {
            var currentPage = Mathf.Max(1, Mathf.CeilToInt(scrollbar.value / scrollbar.size));
            return currentPage;
        }

        private static int GetTotalPages(Scrollbar scrollbar)
        {
            var scrollbarSize = scrollbar.size;
            var totalPages = Mathf.CeilToInt(1f / scrollbarSize);
            return totalPages;
        }
    }
}