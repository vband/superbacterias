﻿using UnityEngine;

namespace SuperbacteriasCode.Utils
{
    public class OpenUrlBehaviour : MonoBehaviour
    {
        [SerializeField] private string _url;

        public void OpenSerializedUrl()
        {
            Application.OpenURL(_url);
        }

        public void OpenUrl(string url)
        {
            Application.OpenURL(url);
        }
    }
}