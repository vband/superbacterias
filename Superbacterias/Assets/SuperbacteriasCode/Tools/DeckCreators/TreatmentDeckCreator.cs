﻿using System.Collections.Generic;
using System.IO;
using SuperbacteriasCode.Models.Cards;
using UnityEngine;

namespace SuperbacteriasCode.Tools.DeckCreators
{
    [CreateAssetMenu(fileName = "TreatmentDeckCreator", menuName = "Superbacterias/DeckCreators/TreatmentDeckCreator")]
    public class TreatmentDeckCreator : DeckCreator
    {
        public override void CreateDeck()
        {
            var cards = new List<Card>();
            
            for (var i = 0; i < _deckDesignSettings.TreatmentNumberOfDuplicates; i++)
            {
                foreach (var color in _gameColors.GetGameColors)
                {
                    for (var power = _deckDesignSettings.TreatmentMinPower; power <= _deckDesignSettings.TreatmentMaxPower; power++)
                    {
                        var treatmentCard = new TreatmentCard(color, power);
                        cards.Add(treatmentCard);
                    }
                }
            }

            for (var i = 0; i < _deckDesignSettings.NumberOfRevealerCards; i++)
            {
                var revealerCard = new RevealerCard();
                cards.Add(revealerCard);
            }
            
            SaveDeckFile(cards);
        }

        protected override string GetFilenameLocation()
        {
            var filenameLocation = Path.Combine(Application.streamingAssetsPath, _deckDesignSettings.TreatmentFileName);
            return filenameLocation;
        }
    }
}