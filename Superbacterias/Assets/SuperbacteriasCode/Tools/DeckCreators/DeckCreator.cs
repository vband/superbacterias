﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SuperbacteriasCode.GameSettings;
using SuperbacteriasCode.Models.Cards;
using UnityEngine;

namespace SuperbacteriasCode.Tools.DeckCreators
{
    public abstract class DeckCreator : ScriptableObject
    {
        [SerializeField] protected GameColors _gameColors;
        [SerializeField] protected DeckDesignSettings _deckDesignSettings;

        public abstract void CreateDeck();

        protected abstract string GetFilenameLocation();

        protected void SaveDeckFile(List<Card> cards)
        {
            var filenameLocation = GetFilenameLocation();
            
            FileStream fs;
            try
            {
                fs = new FileStream(filenameLocation, FileMode.Open);
            }
            catch (Exception)
            {
                fs = File.Create(filenameLocation);
            }

            var formatter = new BinaryFormatter();
            formatter.Serialize(fs, cards);
            fs.Close();
            Debug.Log($"Deck file created at {filenameLocation} with {cards.Count} cards.");
        }
    }
}