﻿using System.Collections.Generic;
using System.IO;
using SuperbacteriasCode.Models.Cards;
using UnityEngine;

namespace SuperbacteriasCode.Tools.DeckCreators
{
    [CreateAssetMenu(fileName = "BacteriaDeckCreator", menuName = "Superbacterias/DeckCreators/BacteriaDeckCreator")]
    public class BacteriaDeckCreator : DeckCreator
    {
        public override void CreateDeck()
        {
            var cards = new List<Card>();
            
            for (var i = 0; i < _deckDesignSettings.BacteriaNumberOfDuplicates; i++)
            {
                foreach (var color in _gameColors.GetGameColors)
                {
                    for (var power = _deckDesignSettings.BacteriaMinPower; power <= _deckDesignSettings.BacteriaMaxPower; power++)
                    {
                        var bacteria = new BacteriaCard(color, power);
                        cards.Add(bacteria);
                    }
                }
            }
            
            SaveDeckFile(cards);
        }
        
        protected override string GetFilenameLocation()
        {
            var filenameLocation = Path.Combine(Application.streamingAssetsPath, _deckDesignSettings.BacteriaFileName);
            return filenameLocation;
        }
    }
}