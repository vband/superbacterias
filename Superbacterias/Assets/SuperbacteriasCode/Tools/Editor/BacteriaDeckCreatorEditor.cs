﻿using SuperbacteriasCode.Tools.DeckCreators;
using UnityEditor;
using UnityEngine;

namespace SuperbacteriasCode.Tools.Editor
{
    [CustomEditor(typeof(BacteriaDeckCreator))]
    public class BacteriaDeckCreatorEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var bacteriaDeckCreator = (BacteriaDeckCreator) target;

            if (GUILayout.Button("Create Deck"))
            {
                bacteriaDeckCreator.CreateDeck();
            }
        }
    }
}