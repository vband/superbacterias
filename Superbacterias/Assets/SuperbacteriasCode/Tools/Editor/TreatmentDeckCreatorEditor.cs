﻿using SuperbacteriasCode.Tools.DeckCreators;
using UnityEditor;
using UnityEngine;

namespace SuperbacteriasCode.Tools.Editor
{
    [CustomEditor(typeof(TreatmentDeckCreator))]
    public class TreatmentDeckCreatorEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var treatmentDeckCreator = (TreatmentDeckCreator) target;

            if (GUILayout.Button("Create Deck"))
            {
                treatmentDeckCreator.CreateDeck();
            }
        }
    }
}