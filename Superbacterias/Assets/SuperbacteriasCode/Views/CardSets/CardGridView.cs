using System.Collections.Generic;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Utils;
using UnityEngine;

namespace SuperbacteriasCode.Views.CardSets
{
    public class CardGridView : CardSetView
    {
        [SerializeField] protected Transform _cardRowPrefab;
        [SerializeField] protected Transform _cardRowsParent;

        [SerializeField] protected int _maxCardsPerRow;
        [SerializeField] protected float _distanceBetweenRows;

        private readonly List<Transform> _cardRows = new List<Transform>();

        public override void UpdateView(List<Card> cards)
        {
            Reset();

            var currentRow = GetNextRow();
            var currentRowChildrenCount = 0;

            foreach (var card in cards)
            {
                if (currentRowChildrenCount >= _maxCardsPerRow)
                {
                    currentRow = GetNextRow();
                    currentRowChildrenCount = 0;
                }
                
                InstantiateNewCardView(card, currentRow);
                currentRowChildrenCount++;
            }
        }

        public override void Reset()
        {
            _cardRows.ForEach(TransformHelper.DestroyChildren);
            _cardRows.Clear();
            _cardViews.Clear();
        }

        private Transform GetNextRow()
        {
            if (_cardRows.Count == 0)
            {
                _cardRows.Add(Instantiate(_cardRowPrefab, _cardRowsParent));
                return _cardRows[0];
            }
            
            var newCardRow = Instantiate(_cardRows[_cardRows.Count - 1], _cardRowsParent);
            _cardRows.Add(newCardRow);
            TransformHelper.DestroyChildren(newCardRow);
            newCardRow.localPosition -= new Vector3(0, _distanceBetweenRows, 0);
            
            return newCardRow;
        }
    }
}
