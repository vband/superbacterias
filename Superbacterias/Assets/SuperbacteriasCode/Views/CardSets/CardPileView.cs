using System.Collections.Generic;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Utils;
using UnityEngine;

namespace SuperbacteriasCode.Views.CardSets
{
    public class CardPileView : CardSetView
    {
        [SerializeField] protected Transform _cardPileTransform;

        public override void UpdateView(List<Card> cards)
        {
            Reset();

            foreach (var card in cards)
            {
                InstantiateNewCardView(card, _cardPileTransform);
            }
        }

        public override void Reset()
        {
            TransformHelper.DestroyChildren(_cardPileTransform);
            _cardViews.Clear();
        }
    }
}
