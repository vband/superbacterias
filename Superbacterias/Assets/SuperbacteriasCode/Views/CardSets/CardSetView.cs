﻿using System.Collections.Generic;
using SuperbacteriasCode.Models;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Views.Cards;
using UnityEngine;

namespace SuperbacteriasCode.Views.CardSets
{
    public abstract class CardSetView : MonoBehaviour
    {
        [SerializeField] protected CardView _cardPrefab;
        [SerializeField] protected CardRepository _cardRepository;

        [SerializeField] [ReadOnly]
        protected List<CardView> _cardViews = new List<CardView>();

        public abstract void UpdateView(List<Card> cards);
        public abstract void Reset();

        public CardView Get(int index)
        {
            if (index < 0 || index >= _cardViews.Count)
                return null;
            
            return _cardViews[index];
        }
        
        protected void InstantiateNewCardView(Card card, Transform cardParent)
        {
            var newCard = Instantiate(_cardPrefab, cardParent);
            newCard.Show(card);
            card.AddObserver(newCard, newCard.UpdateView);
            _cardViews.Add(newCard);
        }
        
        protected void OnEnable()
        {
            if (!_cardRepository)
                return;
            
            _cardRepository.OnModifyRepository += UpdateView;
        }

        protected void OnDisable()
        {
            if (!_cardRepository)
                return;

            _cardRepository.OnModifyRepository -= UpdateView;
        }
    }
}