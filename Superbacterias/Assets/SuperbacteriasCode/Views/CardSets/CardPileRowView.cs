﻿using System.Collections;
using System.Collections.Generic;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Utils;
using UnityEngine;

namespace SuperbacteriasCode.Views.CardSets
{
    public class CardPileRowView : CardSetView
    {
        [SerializeField] protected CardPileView _cardPilePrefab;
        [SerializeField] protected Transform _cardPilesParent;
        
        [SerializeField] [ReadOnly]
        private List<CardPileView> _cardPiles = new List<CardPileView>();

        public override void UpdateView(List<Card> cards)
        {
            Reset();
            
            var currentPileCards = new List<Card>();
            StartNewPile(currentPileCards);

            foreach (var card in cards)
            {
                if (currentPileCards.Count == 0
                    || card.Equals(currentPileCards[currentPileCards.Count - 1]))
                {
                    currentPileCards.Add(card);
                    continue;
                }

                UpdateLastPileView(currentPileCards);
                StartNewPile(currentPileCards);
                currentPileCards.Add(card);
            }
            
            UpdateLastPileView(currentPileCards);
        }

        public override void Reset()
        {
            _cardPiles.ForEach(cardPile => cardPile.Reset());
            _cardPiles.Clear();
            TransformHelper.DestroyChildren(_cardPilesParent);
        }

        private void UpdateLastPileView(List<Card> currentPileCards)
        {
            _cardPiles[_cardPiles.Count - 1].UpdateView(currentPileCards);
        }

        private void StartNewPile(IList currentPileCards)
        {
            currentPileCards.Clear();
            var newPile = Instantiate(_cardPilePrefab, _cardPilesParent);
            _cardPiles.Add(newPile);
        }
    }
}