﻿using System.Threading.Tasks;
using SuperbacteriasCode.Utils;
using UnityEngine;

namespace SuperbacteriasCode.Views.Cards
{
    public abstract class CombatantCardView : CardView
    {
        [SerializeField] protected Animator _animator;
        [SerializeField] protected string _combatAnimation;
        [SerializeField] protected string _deathAnimation;

        public async Task PlayCombatAnimationAsync()
        {
            await AnimatorTaskManager.PlayAsync(_animator, _combatAnimation);
        }

        public async Task PlayDeathAnimationAsync()
        {
            await AnimatorTaskManager.PlayAsync(_animator, _deathAnimation);
        }
    }
}