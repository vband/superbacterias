﻿using UnityEngine;
using UnityEngine.Events;

namespace SuperbacteriasCode.Views.Cards
{
    public class TreatmentCardView : CombatantCardView
    {
        [SerializeField] private Sprite _revealerCardIcon;

        [SerializeField] private UnityEvent OnRevealerCardUsed;

        public Sprite GetRevealerCardIcon()
        {
            return _revealerCardIcon;
        }

        public void InvokeOnRevealerCardUsed()
        {
            OnRevealerCardUsed.Invoke();
        }
    }
}