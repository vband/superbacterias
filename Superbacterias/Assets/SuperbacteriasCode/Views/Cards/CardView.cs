﻿using System.Collections.Generic;
using SuperbacteriasCode.Models.Cards;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SuperbacteriasCode.Views.Cards
{ 
    public class CardView : MonoBehaviour
    {
        public List<Image> ColoredImages = new List<Image>();
        public Image MainIcon;
        public TextMeshProUGUI PowerText;

        protected Card _card;

        public Card Card => _card;

        public virtual void Show(Card card)
        {
            card.Show(this);
            _card = card;
        }

        public virtual void UpdateView()
        {
            Show(_card);
        }

        private void OnDisable()
        {
            _card?.StopObserving(this, UpdateView);
        }
    }
}