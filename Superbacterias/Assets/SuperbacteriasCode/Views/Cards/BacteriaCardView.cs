﻿using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Models.Mutations;
using SuperbacteriasCode.Views.Mutations;
using UnityEngine;

namespace SuperbacteriasCode.Views.Cards
{
    public class BacteriaCardView : CombatantCardView
    {
        [SerializeField] private GameSettings.GameSettings _gameSettings;
        [SerializeField] private MutationListView _mutationListView;

        public string GetUnknownPowerString()
        {
            return _gameSettings.UnknownPowerString;
        }

        public override void Show(Card card)
        {
            base.Show(card);

            if (!(card is BacteriaCard bacteriaCard))
                return;
            
            _mutationListView.ShowMutations(bacteriaCard.Mutations);
        }

        public void ShowMutation(Mutation mutation)
        {
            _mutationListView.ShowMutation(mutation);
        }
    }
}