﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace SuperbacteriasCode.Views
{
    public class ScrollViewBySteps : MonoBehaviour
    {
        [SerializeField] private Transform _content;
        [SerializeField] private int _elementsPerPage;
        [SerializeField] private Scrollbar _scrollbar;
        [SerializeField] private Button _positiveButton;
        [SerializeField] private Button _negativeButton;

        private void OnEnable()
        {
            _positiveButton.onClick.AddListener(() => PerformStep(true));
            _negativeButton.onClick.AddListener(() => PerformStep(false));
        }

        public void UpdateScrollValueAfterElementsChanged()
        {
            var currentValue = _scrollbar.value;
            var pace = GetPace();

            if (pace == 0)
                return;

            for (float i = 0; i <= 1; i += pace)
            {
                if (currentValue > i
                    && currentValue - i >= pace)
                    continue;

                if (currentValue.Equals(i))
                    return;

                if (currentValue > i)
                {
                    _scrollbar.value = Mathf.Clamp01(i);
                    return;
                }

                _scrollbar.value = Mathf.Clamp01(i - pace);
            }
        }

        private float GetPace()
        {
            var elementsCount = GetElementsCount();

            if (elementsCount <= _elementsPerPage * 2)
                return 1;

            var fullPagesCount = elementsCount / _elementsPerPage;
            var eachFullPageSize = 1 / (float) (fullPagesCount - 1);

            var lastPageElementsCount = elementsCount % _elementsPerPage;

            if (lastPageElementsCount == 0)
                return eachFullPageSize;
            
            var ratio = lastPageElementsCount / (float) (elementsCount - _elementsPerPage);
            eachFullPageSize *= 1 - ratio;
            return eachFullPageSize;
        }

        private int GetElementsCount()
        {
            var childCount = 0;

            for (var i = 0; i < _content.childCount; i++)
            {
                var child = _content.GetChild(i);
                
                if (child.parent.Equals(_content))
                    childCount++;
            }

            return childCount;
        }

        private void PerformStep(bool positive)
        {
            var currentValue = _scrollbar.value;
            var pace = GetPace();

            if (pace == 0)
                return;
            
            var direction = positive ? 1 : -1;

            var elementsCount = GetElementsCount();
            if (Math.Abs(pace - 1) > 0.01f
                && !positive
                && Math.Abs(currentValue - 1) < 0.01f
                && elementsCount % _elementsPerPage != 0)
            {
                _scrollbar.value = (Mathf.FloorToInt(elementsCount / (float) _elementsPerPage) - 1) * pace;
                return;
            }
            
            _scrollbar.value = Mathf.Clamp01(currentValue + direction * pace);
        }
    }
}