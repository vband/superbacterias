﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SuperbacteriasCode.Views.Mutations
{
    public class MutationView : MonoBehaviour
    {
        [SerializeField] private Image _knownImage;
        [SerializeField] private Image _unknownImage;
        [SerializeField] private TextMeshProUGUI _textMeshPro;
        
        [SerializeField] private Sprite _unkownMutationSprite;
        
        [SerializeField] private Sprite _firstColorMutationSprite;
        [SerializeField] private Sprite _secondColorMutationSprite;
        [SerializeField] private Sprite _thirdColorMutationSprite;
        [SerializeField] private Sprite _fourthColorMutationSprite;

        public Image KnownImage => _knownImage;
        public Image UnknownImage => _unknownImage;

        public TextMeshProUGUI TextMeshPro => _textMeshPro;

        public Sprite UnkownMutationSprite => _unkownMutationSprite;
        
        public Sprite FirstColorMutationSprite => _firstColorMutationSprite;
        public Sprite SecondColorMutationSprite => _secondColorMutationSprite;
        public Sprite ThirdColorMutationSprite => _thirdColorMutationSprite;
        public Sprite FourthColorMutationSprite => _fourthColorMutationSprite;
    }
}