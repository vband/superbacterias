﻿using System.Collections.Generic;
using SuperbacteriasCode.Models.Mutations;
using SuperbacteriasCode.Utils;
using UnityEngine;

namespace SuperbacteriasCode.Views.Mutations
{
    public class MutationListView : MonoBehaviour
    {
        [SerializeField] private MutationView _mutationViewPrefab;
        [SerializeField] private Transform _layoutGroupTransform;
        [SerializeField] private Transform _colorMutationsTransform;

        private readonly List<MutationView> _mutationViews = new List<MutationView>();

        public int ShownColorMutationsCount { get; set; }

        public void ShowMutation(Mutation mutation)
        {
            mutation.Show(this);
        }

        public void ShowMutations(List<Mutation> mutations)
        {
            TransformHelper.DestroyChildrenExcept(transform, _layoutGroupTransform);
            TransformHelper.DestroyChildren(_layoutGroupTransform);
            
            ShownColorMutationsCount = 0;
            
            mutations.ForEach(ShowMutation);
        }

        public MutationView InstantiateNewMutationView(bool isKnownColorMutation)
        {
            var newMutationView = Instantiate(_mutationViewPrefab,
                isKnownColorMutation ? _colorMutationsTransform : _layoutGroupTransform);
            
            _mutationViews.Add(newMutationView);
            
            return newMutationView;
        }
    }
}