﻿using DoubleDash.CodingTools.ReactiveVariables;
using TMPro;
using UnityEngine;

namespace SuperbacteriasCode.Views.ReactiveScriptables
{
    public abstract class ReactiveScriptableView<Type> : MonoBehaviour
    {
        [SerializeField] protected TextMeshProUGUI _text;
        [SerializeField] protected ReactiveVariableScriptable<Type> _reactiveVariable;
        
        protected void OnEnable()
        {
            _reactiveVariable.Subscribe(OnValueChange);
            _reactiveVariable.OnValueChangedEvent.Trigger();
        }
        
        private void OnValueChange<T>(T value)
        {
            _text.text = value.ToString();
        }
    }
}