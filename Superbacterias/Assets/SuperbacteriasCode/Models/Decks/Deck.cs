﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SuperbacteriasCode.GameSettings;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Utils;
using UnityEngine;
using UnityEngine.Networking;

namespace SuperbacteriasCode.Models.Decks
{
    public abstract class Deck : ScriptableObject
    {
        [SerializeField] protected DeckDesignSettings _deckDesignSettings;

        protected List<Card> _cards;

        protected abstract string GetFilenameLocation();

        public void InitDeck()
        {
            var filenameLocation = GetFilenameLocation();
            Debug.Log($"filenameLocation = {filenameLocation}");
            var formatter = new BinaryFormatter();
            Debug.Log($"formatter = {formatter}");
            
#if !UNITY_EDITOR
            var webRequest = UnityWebRequest.Get(filenameLocation);
            Debug.Log($"webRequest = {webRequest}");
            webRequest.SendWebRequest();

            Debug.Log($"webRequest.result = {webRequest.result}");
            if (webRequest.result is UnityWebRequest.Result.ConnectionError
                or UnityWebRequest.Result.ProtocolError)
            {
                Debug.LogError(webRequest.error);
                return;
            }
            
            Debug.Log($"webRequest.downloadHandler = {webRequest.downloadHandler}");
            Debug.Log($"webRequest.downloadHandler.data = {webRequest.downloadHandler.data}");
            var stream = new MemoryStream(webRequest.downloadHandler.data);
            Debug.Log($"stream = {stream}");
            _cards = (List<Card>) formatter.Deserialize(stream);
            
            Debug.Log($"_cards.Count = {_cards.Count}");
            ShuffleDeck();
            return;
#endif
            try
            {
                var fs = new FileStream(filenameLocation, FileMode.Open);
                _cards = (List<Card>) formatter.Deserialize(fs);
                fs.Close();
                ShuffleDeck();
            }
            catch (DirectoryNotFoundException)
            {
                Debug.LogError($"Unable to initialize deck. Directory not found: {filenameLocation}");
            }
        }

        public void ShuffleDeck()
        {
            _cards.ShuffleList();
        }

        public Card TakeFromTop()
        {
            if (IsEmpty())
            {
                _cards.Clear();
                InitDeck();
            }
            
            var card = _cards[0];
            _cards.Remove(card);
            return card;
        }

        public bool TryLookAtTop(out Card card)
        {
            if (IsEmpty())
            {
                card = null;
                return false;
            }

            card = _cards[0];
            return true;
        }

        public void InsertToBottom(Card card)
        {
            _cards.Insert(_cards.Count, card);
        }

        private bool IsEmpty()
        {
            return _cards.Count == 0;
        }
    }
}