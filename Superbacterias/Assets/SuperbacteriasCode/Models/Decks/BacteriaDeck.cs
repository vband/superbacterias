﻿using System.IO;
using UnityEngine;

namespace SuperbacteriasCode.Models.Decks
{
    [CreateAssetMenu(fileName = "BacteriaDeck", menuName = "Superbacterias/Decks/BacteriaDeck")]
    public class BacteriaDeck : Deck
    {
        protected override string GetFilenameLocation()
        {
            var filenameLocation = Path.Combine(Application.streamingAssetsPath, _deckDesignSettings.BacteriaFileName);
            return filenameLocation;
        }
    }
}