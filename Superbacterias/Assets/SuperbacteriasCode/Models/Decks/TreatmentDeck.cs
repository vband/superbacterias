﻿using System.IO;
using UnityEngine;

namespace SuperbacteriasCode.Models.Decks
{
    [CreateAssetMenu(fileName = "TreatmentDeck", menuName = "Superbacterias/Decks/TreatmentDeck")]
    public class TreatmentDeck : Deck
    {
        protected override string GetFilenameLocation()
        {
            var filenameLocation = Path.Combine(Application.streamingAssetsPath, _deckDesignSettings.TreatmentFileName);
            return filenameLocation;
        }
    }
}