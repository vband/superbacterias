﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DoubleDash.CodingTools.ReactiveVariables;
using SuperbacteriasCode.Views.Cards;
using UnityEngine;

namespace SuperbacteriasCode.Models.Cards
{
    [Serializable]
    public class CombatantCard : Card
    {
        public Color Color;
        public int Power;

        [NonSerialized]
        protected CardView _cardView;
        
        public CardView CardView => _cardView;

        public override void Show(CardView cardView)
        {
            ShowColor(cardView);
            ShowPower(cardView);

            _cardView = cardView;
        }

        protected void ShowPower(CardView cardView)
        {
            cardView.PowerText.text = Power.ToString();
        }

        protected void ShowColor(CardView cardView)
        {
            if (Color.TryGetUnityColor(out var color))
            {
                cardView.ColoredImages.ForEach(coloredImage => coloredImage.color = color);
            }
        }

        protected void ShowUnkownColor(CardView cardView)
        {
            cardView.ColoredImages.ForEach(coloredImage => coloredImage.color = UnityEngine.Color.gray);
        }

        public override async Task UseOnBacterias(CardRepository bacterias)
        {
            for (var i = 0; i < bacterias.Count(); i++)
            {
                var bacteria = bacterias.Get(i) as BacteriaCard;

                await AttackBacteria(bacteria);

                if (Power <= 0)
                    break;
            }

            Power = 0;
            
            if (_cardView is CombatantCardView combatantCardView)
            {
                await combatantCardView.PlayDeathAnimationAsync();
            }
        }

        // Retorna o número de cartas já puxadas do baralho na fase de compra, e se era uma carta surge
        public override async Task<Tuple<int, bool>> OnDrawFromDeck(CardRepository bacterias, List<Card> buyPhaseCards, int numberOfDrawnCards)
        {
            await Task.Delay(200);

            return new Tuple<int, bool>(numberOfDrawnCards, false); // Não altera a contagem de cartas puxadas do baralho
        }

        public override bool ShouldBeDrawnFromDeckNow(IntReactiveScriptable turnCount, GameSettings.GameSettings gameSettings)
        {
            return true;
        }

        private async Task AttackBacteria(BacteriaCard bacteriaCard)
        {
            if (Power <= 0)
                return;

            bacteriaCard.RevealPower();
            bacteriaCard.InvokeOnModifiedCallbacks();
            await WaitCombatAnimations(bacteriaCard);
            
            ResolveCombat(bacteriaCard, out var sameColor);
            bacteriaCard.InvokeOnModifiedCallbacks();
            InvokeOnModifiedCallbacks();

            if (sameColor)
            {
                bacteriaCard.RevealColor();
                bacteriaCard.InvokeOnModifiedCallbacks();
            }

            await WaitDeathAnimation(bacteriaCard);
        }

        private async Task WaitCombatAnimations(BacteriaCard bacteriaCard)
        {
            var bacteriaCombatantCardView = bacteriaCard._cardView as CombatantCardView;
            if (bacteriaCombatantCardView != null)
            {
#pragma warning disable CS4014
                bacteriaCombatantCardView.PlayCombatAnimationAsync();
#pragma warning restore CS4014
            }
            
            if (_cardView is CombatantCardView combatantCardView)
            {
                await combatantCardView.PlayCombatAnimationAsync();
            }
        }

        private static async Task WaitDeathAnimation(BacteriaCard bacteriaCard)
        {
            var bacteriaCombatantCardView = bacteriaCard._cardView as CombatantCardView;
            
            if (bacteriaCard.IsDead() && bacteriaCombatantCardView != null)
            {
                await bacteriaCombatantCardView.PlayDeathAnimationAsync();
            }
        }

        private void ResolvePower(CombatantCard defender, int defenderPowerModifier)
        {
            var defenderFinalPower = Mathf.Max(0, defender.Power + defenderPowerModifier);
            
            if (Power > defenderFinalPower)
            {
                Power = Mathf.Max(0, Power - defenderFinalPower);
                defender.Power = 0;
            }
            else if (Power < defenderFinalPower)
            {
                defender.Power = Mathf.Max(0, defender.Power - Power);
                Power = 0;
            }
            else
            {
                Power = 0;
                defender.Power = 0;
            }
        }

        private void ResolveCombat(BacteriaCard defender, out bool sameColor)
        {
            sameColor = false;
            var defenderPowerModifier = 0;

            if (defender.Color.Equals(Color))
            {
                sameColor = true;
                return;
            }

            foreach (var mutation in defender.Mutations)
            {
                mutation.AffectCombat(this, out var mutationSameColor, out var mutationPowerModifier);

                defenderPowerModifier += mutationPowerModifier;

                if (mutationSameColor)
                {
                    sameColor = true;
                    return;
                }
            }

            ResolvePower(defender, defenderPowerModifier);
        }

        public bool IsDead()
        {
            return Power <= 0;
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj))
                return false;

            if (!(obj is CombatantCard other))
                return false;

            return Power == other.Power && Color.Id.Equals(other.Color.Id);
        }

        public override int GetHashCode()
        {
            return Power ^ Color.Id.GetHashCode();
        }
    }
}