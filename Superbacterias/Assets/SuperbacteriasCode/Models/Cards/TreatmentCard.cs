﻿namespace SuperbacteriasCode.Models.Cards
{
    [System.Serializable]
    public class TreatmentCard : CombatantCard
    {
        public TreatmentCard(Color color, int power)
        {
            Color = color;
            Power = power;
        }
    }
}