﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SuperbacteriasCode.Models.Cards
{
    [Serializable]
    public abstract class SurgeCard : Card
    {
        // Retorna o número de cartas já puxadas do baralho na fase de compra
        public override async Task<Tuple<int, bool>> OnDrawFromDeck(CardRepository bacterias, List<Card> buyPhaseCards, int numberOfDrawnCards)
        {
            await UseOnBacterias(bacterias);
            buyPhaseCards.Remove(this);
            return new Tuple<int, bool>(numberOfDrawnCards - 1, true);
        }
    }
}