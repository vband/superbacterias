﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DoubleDash.CodingTools.ReactiveVariables;
using SuperbacteriasCode.Views.Cards;

namespace SuperbacteriasCode.Models.Cards
{
    [Serializable]
    public abstract class Card
    {
        protected List<(object, Action)> _observers = new List<(object, Action)>();

        public abstract void Show(CardView cardView);

        public abstract Task UseOnBacterias(CardRepository bacterias);

        // Retorna o número de cartas já puxadas do baralho na fase de compra, e foi uma carta surge
        public abstract Task<Tuple<int, bool>> OnDrawFromDeck(CardRepository bacterias, List<Card> buyPhaseCards, int numberOfDrawnCards);

        public abstract bool ShouldBeDrawnFromDeckNow(IntReactiveScriptable turnCount,
            GameSettings.GameSettings gameSettings);

        public override bool Equals(object obj)
        {
            return obj != null && GetType() == obj.GetType();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public void AddObserver(object obj, Action onModifiedCallback)
        {
            if (_observers.Contains((obj, onModifiedCallback)))
                return;
            
            _observers.Add((obj, onModifiedCallback));
        }

        public void StopObserving(object obj, Action onModifiedCallback)
        {
            if (!_observers.Contains((obj, onModifiedCallback)))
                return;

            _observers.Remove((obj, onModifiedCallback));
        }

        public void InvokeOnModifiedCallbacks()
        {
            _observers.ForEach(observer => observer.Item2.Invoke());
        }
    }
}