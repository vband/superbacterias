﻿using System;
using System.Threading.Tasks;
using DoubleDash.CodingTools.ReactiveVariables;
using SuperbacteriasCode.Views.Cards;

namespace SuperbacteriasCode.Models.Cards
{
    [Serializable]
    public class RevealerCard : SurgeCard
    {
        [NonSerialized]
        private CardView _cardView;
        
        public override void Show(CardView cardView)
        {
            _cardView = cardView;
            
            cardView.PowerText.enabled = false;
            cardView.ColoredImages.ForEach(coloredImage => coloredImage.color = UnityEngine.Color.white);

            var treatmentCardView = cardView as TreatmentCardView;
            if (treatmentCardView == null)
                return;

            treatmentCardView.MainIcon.sprite = treatmentCardView.GetRevealerCardIcon();
        }

        public override async Task UseOnBacterias(CardRepository bacterias)
        {
            if (bacterias.Count() == 0)
                return;

            var leftmostUnrevealedBacteria = GetLeftmostUnrevealedBacteria(bacterias);

            if (leftmostUnrevealedBacteria == null)
                return;
            
            leftmostUnrevealedBacteria.RevealAll();
            leftmostUnrevealedBacteria.InvokeOnModifiedCallbacks();

            await WaitAnimations(leftmostUnrevealedBacteria);

            if (_cardView is TreatmentCardView treatmentCardView)
            {
                treatmentCardView.InvokeOnRevealerCardUsed();
            }
        }

        private async Task WaitAnimations(BacteriaCard leftmostUnrevealedBacteria)
        {
            if (leftmostUnrevealedBacteria.CardView is CombatantCardView bacteriaCombatantCardView)
            {
#pragma warning disable CS4014
                bacteriaCombatantCardView.PlayCombatAnimationAsync();
#pragma warning restore CS4014
            }

            if (_cardView is CombatantCardView treatmentCombatantCardView)
            {
                await treatmentCombatantCardView.PlayCombatAnimationAsync();
            }
        }

        public override bool ShouldBeDrawnFromDeckNow(IntReactiveScriptable turnCount, GameSettings.GameSettings gameSettings)
        {
            return turnCount.Value >= gameSettings.MinTurnsForRevealerCard;
        }

        private BacteriaCard GetLeftmostUnrevealedBacteria(CardRepository bacterias)
        {
            var i = -1;
            while (i < bacterias.Count() - 1)
            {
                i++;

                if (!(bacterias.Get(i) is BacteriaCard bacteria))
                    continue;

                if (!bacteria.IsFullyRevealed())
                    return bacteria;
            }

            return null;
        }
    }
}