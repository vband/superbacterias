﻿using System.Collections.Generic;
using SuperbacteriasCode.Models.Mutations;
using SuperbacteriasCode.Views.Cards;

namespace SuperbacteriasCode.Models.Cards
{
    [System.Serializable]
    public class BacteriaCard : CombatantCard
    {
        public List<Mutation> Mutations = new();
        
        public bool Newborn;

        protected bool _isPowerKnown;
        protected bool _isColorKnown;

        public bool IsPowerKnown => _isPowerKnown;
        public bool IsColorKnown => _isColorKnown;

        public BacteriaCard(Color color, int power)
        {
            Color = color;
            Power = power;
        }

        public BacteriaCard(Color color, int power, List<Mutation> mutations, bool isPowerKnown, bool isColorKnown)
        {
            Color = color;
            Power = power;
            mutations.ForEach(mutation => Mutations.Add(mutation));
            _isPowerKnown = isPowerKnown;
            _isColorKnown = isColorKnown;
        }

        public BacteriaCard ProduceChild()
        {
            return new BacteriaCard(Color, Power, Mutations, IsPowerKnown, IsColorKnown)
            {
                Newborn = true
            };
        }
        
        public override void Show(CardView cardView)
        {
            if (_isPowerKnown)
            {
                ShowPower(cardView);
            }
            else
            {
                ShowUnknownPower(cardView);
            }

            if (_isColorKnown)
            {
                ShowColor(cardView);
            }
            else
            {
                ShowUnkownColor(cardView);
            }

            _cardView = cardView;
        }

        private void ShowUnknownPower(CardView cardView)
        {
            var bacteriaCardView = cardView as BacteriaCardView;

            if (bacteriaCardView == null)
                return;

            bacteriaCardView.PowerText.text = bacteriaCardView.GetUnknownPowerString();
        }

        public void RevealColor()
        {
            _isColorKnown = true;
        }

        public void RevealPower()
        {
            _isPowerKnown = true;
        }

        public void RevealMutations()
        {
            Mutations.ForEach(mutation => mutation.IsKnown = true);
        }

        public void RevealAll()
        {
            RevealColor();
            RevealPower();
            RevealMutations();
        }

        public bool IsFullyRevealed()
        {
            return _isPowerKnown
                   && _isColorKnown
                   && Mutations.All(mutation => mutation.IsKnown);
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj))
                return false;

            if (!(obj is BacteriaCard other))
                return false;

            if (Mutations.Count != other.Mutations.Count)
                return false;

            for (var i = 0; i < Mutations.Count; i++)
            {
                if (!Mutations[i].Equals(other.Mutations[i]))
                    return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            var hashCode = base.GetHashCode();
            
            hashCode ^= _isColorKnown.GetHashCode() ^ _isPowerKnown.GetHashCode();

            foreach (var mutation in Mutations)
                hashCode ^= mutation.GetHashCode();

            return hashCode;
        }
    }
}