﻿using System;
using UnityEngine;

namespace SuperbacteriasCode.Models
{
    [Serializable]
    public class Color : IEquatable<Color>
    {
        public string Id;
        public string HtmlColorString;

        public bool Equals(Color other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && HtmlColorString == other.HtmlColorString;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Color) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Id != null ? Id.GetHashCode() : 0) * 397) ^ (HtmlColorString != null ? HtmlColorString.GetHashCode() : 0);
            }
        }

        public bool TryGetUnityColor(out UnityEngine.Color color)
        {
            return ColorUtility.TryParseHtmlString(HtmlColorString, out color);
        }
    }
}