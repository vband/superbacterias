﻿using System;
using System.Collections.Generic;
using System.Linq;
using SuperbacteriasCode.Models.Cards;
using UnityEngine;
using UnityEngine.Events;

namespace SuperbacteriasCode.Models
{
    [CreateAssetMenu(fileName = "NewCardRepository", menuName = "Superbacterias/CardRepository", order = 0)]
    public class CardRepository : ScriptableObject
    {
        private List<Card> _cards { get; } = new List<Card>();

        public delegate void OnModifyRepositoryDelegate(List<Card> cards);
        public OnModifyRepositoryDelegate OnModifyRepository;

        [SerializeField] private UnityEvent<int> _onRepositoryCountChanged;
        [SerializeField] private UnityEvent<List<Card>> _onRepositoryListChanged;

        public void Clear()
        {
            _cards.Clear();
        }
        
        public Card Get(int index)
        {
            return _cards[index];
        }

        public int Count()
        {
            return _cards.Count;
        }

        public int Count(Func<Card, bool> condition)
        {
            return _cards.Count(condition);
        }

        public void Add(Card card)
        {
            _cards.Add(card);

            NotifyRepositoryModified();
        }

        public void Insert(int index, Card card)
        {
            _cards.Insert(index, card);
            
            NotifyRepositoryModified();
        }

        public void Remove(Card card)
        {
            _cards.Remove(card);

            NotifyRepositoryModified();
        }

        public void RemoveAt(int index)
        {
            _cards.RemoveAt(index);

            NotifyRepositoryModified();
        }

        public int IndexOf(Card card)
        {
            return _cards.IndexOf(card);
        }

        public void NotifyRepositoryModified()
        {
            OnModifyRepository?.Invoke(_cards);
            
            _onRepositoryCountChanged.Invoke(_cards.Count);
            _onRepositoryListChanged.Invoke(_cards);
        }
    }
}