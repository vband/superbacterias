﻿using System.Threading.Tasks;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Views;
using SuperbacteriasCode.Views.Cards;
using SuperbacteriasCode.Views.Mutations;

namespace SuperbacteriasCode.Models.Mutations
{
    public class NeutralMutation : Mutation
    {
        public override async Task RegisterMutationAsync( BacteriaCard bacteria, BacteriaCardView bacteriaCardView)
        {
            bacteria.Mutations.Add(this);
            
            if (bacteriaCardView is null)
                return;

            bacteriaCardView.ShowMutation(this);
            
            //TODO: Animação
            await Task.Delay(500);
        }

        public override void AffectCombat(CombatantCard attacker, out bool mutationSameColor, out int defenderPowerModifier)
        {
            mutationSameColor = false;
            defenderPowerModifier = 0;
            IsKnown = true;
        }

        public override void Show(MutationListView mutationListView)
        {
            var newMutationView = mutationListView.InstantiateNewMutationView(false);
            
            if (IsKnown)
            {
                newMutationView.KnownImage.enabled = false;
                newMutationView.UnknownImage.enabled = false;
                newMutationView.TextMeshPro.enabled = true;
                newMutationView.TextMeshPro.text = "+0";
            }
            else
            {
                newMutationView.KnownImage.enabled = false;
                newMutationView.TextMeshPro.enabled = false;
                newMutationView.UnknownImage.enabled = true;
                newMutationView.UnknownImage.sprite = newMutationView.UnkownMutationSprite;
            }
        }
    }
}