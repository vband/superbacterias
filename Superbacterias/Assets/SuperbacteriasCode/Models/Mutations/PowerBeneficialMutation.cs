﻿using System.Threading.Tasks;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Views;
using SuperbacteriasCode.Views.Cards;
using SuperbacteriasCode.Views.Mutations;

namespace SuperbacteriasCode.Models.Mutations
{
    public class PowerBeneficialMutation : BeneficialMutation
    {
        public int PowerAdded { get; }

        public PowerBeneficialMutation(int powerAdded)
        {
            PowerAdded = powerAdded;
        }
        
        public override async Task RegisterMutationAsync(BacteriaCard bacteria, BacteriaCardView bacteriaCardView)
        {
            bacteria.Mutations.Add(this);

            IsKnown = true;

            if (bacteriaCardView is null)
                return;

            bacteriaCardView.ShowMutation(this);
            
            //TODO: Animação
            await Task.Delay(500);
        }

        public override void AffectCombat(CombatantCard attacker, out bool mutationSameColor, out int defenderPowerModifier)
        {
            defenderPowerModifier = PowerAdded;
            mutationSameColor = false;
            IsKnown = true;
        }

        public override void Show(MutationListView mutationListView)
        {
            var newMutationView = mutationListView.InstantiateNewMutationView(false);
            
            if (IsKnown)
            {
                newMutationView.KnownImage.enabled = false;
                newMutationView.UnknownImage.enabled = false;
                newMutationView.TextMeshPro.enabled = true;
                newMutationView.TextMeshPro.text = "+" + PowerAdded;
            }
            else
            {
                newMutationView.KnownImage.enabled = false;
                newMutationView.TextMeshPro.enabled = false;
                newMutationView.UnknownImage.enabled = true;
                newMutationView.UnknownImage.sprite = newMutationView.UnkownMutationSprite;
            }
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj))
                return false;

            if (!(obj is PowerBeneficialMutation other))
                return false;

            return PowerAdded == other.PowerAdded;
        }
        
        public override int GetHashCode()
        {
            return PowerAdded ^ IsKnown.GetHashCode();
        }
    }
}