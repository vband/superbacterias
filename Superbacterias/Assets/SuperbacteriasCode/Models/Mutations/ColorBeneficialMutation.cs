﻿using System.Threading.Tasks;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Views;
using SuperbacteriasCode.Views.Cards;
using SuperbacteriasCode.Views.Mutations;
using UnityEngine;
using UnityEngine.UI;

namespace SuperbacteriasCode.Models.Mutations
{
    public class ColorBeneficialMutation : BeneficialMutation
    {
        public Color ColorAdded { get; }

        public ColorBeneficialMutation(Color colorAdded)
        {
            ColorAdded = colorAdded;
        }

        public override async Task RegisterMutationAsync(BacteriaCard bacteria, BacteriaCardView bacteriaCardView)
        {
            bacteria.Mutations.Add(this);
            
            if (bacteriaCardView is null)
                return;

            bacteriaCardView.ShowMutation(this);
            
            //TODO: Animação
            await Task.Delay(500);
        }

        public override void AffectCombat(CombatantCard attacker, out bool mutationSameColor, out int defenderPowerModifier)
        {
            defenderPowerModifier = 0;
            mutationSameColor = attacker.Color.Equals(ColorAdded);

            if (mutationSameColor)
                IsKnown = true;
        }

        public override void Show(MutationListView mutationListView)
        {
            if (IsKnown)
            {
                var newMutationView = mutationListView.InstantiateNewMutationView(true);
                newMutationView.TextMeshPro.enabled = false;
                newMutationView.UnknownImage.enabled = false;
                newMutationView.KnownImage.enabled = true;

                switch (mutationListView.ShownColorMutationsCount)
                {
                    case 0:
                        // 2 segmentos da mesma cor (first, second)
                        newMutationView.KnownImage.sprite = newMutationView.FirstColorMutationSprite;
                        SetImageColor(newMutationView);
                        CreateNewImageWithSprite(newMutationView.KnownImage.gameObject, newMutationView.SecondColorMutationSprite);
                        break;
                    
                    case 1:
                        // 2 segmentos da mesma cor (second e third)
                        newMutationView.KnownImage.sprite = newMutationView.SecondColorMutationSprite;
                        SetImageColor(newMutationView);
                        CreateNewImageWithSprite(newMutationView.KnownImage.gameObject, newMutationView.ThirdColorMutationSprite);
                        break;
                    
                    case 2:
                        // 1 segmento (third)
                        newMutationView.KnownImage.sprite = newMutationView.ThirdColorMutationSprite;
                        SetImageColor(newMutationView);
                        break;
                }

                mutationListView.ShownColorMutationsCount++;
            }
            else
            {
                var newMutationView = mutationListView.InstantiateNewMutationView(false);
                newMutationView.TextMeshPro.enabled = false;
                newMutationView.KnownImage.enabled = false;
                newMutationView.UnknownImage.enabled = true;
                newMutationView.UnknownImage.sprite = newMutationView.UnkownMutationSprite;
            }
        }

        private void SetImageColor(MutationView newMutationView)
        {
            if (ColorAdded.TryGetUnityColor(out var color))
                newMutationView.KnownImage.color = color;
        }

        private static void CreateNewImageWithSprite(GameObject originalImage, Sprite sprite)
        {
            var newImage = Object.Instantiate(originalImage, originalImage.transform.parent);
            newImage.GetComponent<Image>().sprite = sprite;
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj))
                return false;
            
            if (!(obj is ColorBeneficialMutation other))
                return false;

            return ColorAdded.Id.Equals(other.ColorAdded.Id);
        }
        
        public override int GetHashCode()
        {
            return ColorAdded.Id.GetHashCode() ^ IsKnown.GetHashCode();
        }
    }
}