﻿using System.Threading.Tasks;
using SuperbacteriasCode.Models.Cards;
using SuperbacteriasCode.Views;
using SuperbacteriasCode.Views.Cards;
using SuperbacteriasCode.Views.Mutations;

namespace SuperbacteriasCode.Models.Mutations
{
    [System.Serializable]
    public abstract class Mutation
    {
        public bool IsKnown;

        // Retorna true se houve mutacao
        public abstract Task RegisterMutationAsync(BacteriaCard bacteria, BacteriaCardView bacteriaCardView);

        public abstract void AffectCombat(CombatantCard attacker, out bool mutationSameColor, out int defenderPowerModifier);

        public abstract void Show(MutationListView mutationListView);

        public override bool Equals(object obj)
        {
            if (!(obj is Mutation other))
                return false;

            return IsKnown == other.IsKnown;
        }

        public override int GetHashCode()
        {
            return IsKnown.GetHashCode();
        }
    }
}