﻿using UnityEngine;

namespace SuperbacteriasCode.GameSettings
{
    [CreateAssetMenu(fileName = "GameSettings", menuName = "Superbacterias/GameSettings", order = 0)]
    public class GameSettings : ScriptableObject
    {
        [SerializeField] private int _cardsShownFirstTurn = 4;
        [SerializeField] private int _cardsChosenFirstTurn = 2;
        [SerializeField] private int _cardsShownLaterTurns = 2;
        [SerializeField] private int _cardsChosenLaterTurns = 1;

        [SerializeField] private int _bacteriasInitialNumber = 2;

        [SerializeField] private string _unknownPowerString = "?";

        [SerializeField] private float _mutationChance = 0.5f;
        [SerializeField] private float _prejudicialMutationChance;
        [SerializeField] private float _neutralMutationChance;
        [SerializeField] private float _benficialMutationChance;
        [SerializeField] private float _colorBeneficialMutationChance;
        [SerializeField] private float _powerBeneficialMutationChance;
        
        [SerializeField] private int _minPowerBeneficialMutationValue = 1;
        [SerializeField] private int _maxPowerBeneficialMutationValue = 3;
        [SerializeField] private int _minPowerPrejudicialMutationValue = 1;
        [SerializeField] private int _maxPowerPrejudicialMutationValue = 3;

        [SerializeField] private int _maxNumberOfBacterias;

        [SerializeField] private int _turnsAfterSuperBacteria = 3;

        [SerializeField] private int _minTurnsForMutation = 3;
        [SerializeField] private int _forcedMutationTurn = 3;

        [SerializeField] private int _minTurnsForRevealerCard = 4;

        [SerializeField] private string _superbacteriaDefeatReasonText;
        [SerializeField] private string _tooManyBacteriasDefeatReasonText;

        public int BacteriasInitialNumber => _bacteriasInitialNumber;
        
        public string UnknownPowerString => _unknownPowerString;

        public float MutationChance => _mutationChance;
        public float PrejudicialMutationChance => _prejudicialMutationChance;
        public float NeutralMutationChance => _neutralMutationChance;
        public float BenficialMutationChance => _benficialMutationChance;
        public float ColorBeneficialMutationChance => _colorBeneficialMutationChance;
        public float PowerBeneficialMutationChance => _powerBeneficialMutationChance;
        
        
        public int MinPowerBeneficialMutationValue => _minPowerBeneficialMutationValue;
        public int MaxPowerBeneficialMutationValue => _maxPowerBeneficialMutationValue;
        public int MinPowerPrejudicialMutationValue => _minPowerPrejudicialMutationValue;
        public int MaxPowerPrejudicialMutationValue => _maxPowerPrejudicialMutationValue;

        public int MaxNumberOfBacterias => _maxNumberOfBacterias;

        public int TurnsAfterSuperBacteria => _turnsAfterSuperBacteria;

        public int MinTurnsForMutation => _minTurnsForMutation;
        public int ForcedMutationTurn => _forcedMutationTurn;

        public int MinTurnsForRevealerCard => _minTurnsForRevealerCard;

        public string SuperbacteriaDefeatReasonText => _superbacteriaDefeatReasonText;
        public string TooManyBacteriasDefeatReasonText => _tooManyBacteriasDefeatReasonText;

        public void GetNumberOfCardsShownAndChosen(int turnCount, out int cardsShown, out int cardsChosen)
        {
            if (turnCount == 1)
            {
                cardsShown = _cardsShownFirstTurn;
                cardsChosen = _cardsChosenFirstTurn;
                return;
            }

            cardsShown = _cardsShownLaterTurns;
            cardsChosen = _cardsChosenLaterTurns;
        }
    }
}