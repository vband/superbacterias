﻿using System.Collections.Generic;
using SuperbacteriasCode.Utils;
using UnityEngine;

namespace SuperbacteriasCode.GameSettings
{
    [CreateAssetMenu(fileName = "GameColors", menuName = "Superbacterias/GameColors")]
    public class GameColors : ScriptableObject
    {
        [SerializeField] private List<Models.Color> _gameColors;

        public List<Models.Color> GetGameColors => _gameColors;

        public Models.Color GetRandomColor()
        {
            return GetGameColors[Random.Range(0, GetGameColors.Count)];
        }

        public Models.Color GetRandomColorExcept(List<Models.Color> colors)
        {
            var colorsToChoose = ListHelper.CopyList(_gameColors);
            colors.ForEach(color => colorsToChoose.Remove(color));

            if (colorsToChoose.Count == 0)
                return null;
            
            return colorsToChoose[Random.Range(0, colorsToChoose.Count)];
        }
    }
}