﻿using UnityEngine;

namespace SuperbacteriasCode.GameSettings
{
    [CreateAssetMenu(fileName = "DeckDesignSettings", menuName = "Superbacterias/DeckDesignSettings", order = 0)]
    public class DeckDesignSettings : ScriptableObject
    {
        [Header("Bacteria Deck")]
        [SerializeField] private int _bacteriaMinPower = 0;
        [SerializeField] private int _bacteriaMaxPower = 0;
        [SerializeField] private int _bacteriaNumberOfDuplicates = 0;
        [SerializeField] private string _bacteriaFileName = "BacteriaDeck.txt";
        
        [Header("Treatment Deck")]
        [SerializeField] private int _treatmentMinPower = 0;
        [SerializeField] private int _treatmentMaxPower = 0;
        [SerializeField] private int _treatmentNumberOfDuplicates = 0;
        [SerializeField] private int _numberOfRevealerCards;
        [SerializeField] private string _treatmentFileName = "TreatmentDeck.txt";

        public int BacteriaMinPower => _bacteriaMinPower;
        public int BacteriaMaxPower => _bacteriaMaxPower;
        public int BacteriaNumberOfDuplicates => _bacteriaNumberOfDuplicates;
        public string BacteriaFileName => _bacteriaFileName;
        public int TreatmentMinPower => _treatmentMinPower;
        public int TreatmentMaxPower => _treatmentMaxPower;
        public int TreatmentNumberOfDuplicates => _treatmentNumberOfDuplicates;
        public int NumberOfRevealerCards => _numberOfRevealerCards;
        public string TreatmentFileName => _treatmentFileName;
    }
}